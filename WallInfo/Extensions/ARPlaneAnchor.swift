//
//  ARPlaneAnchor.swift
//  WallInfo
//
//  Created by Dan Ridzon on 06.04.2023.
//

import ARKit
import RealityKit

@available(iOS 12.0, *)
extension ARPlaneAnchor {
    
    // Gives values to classification types, so plane anchors can be sorted by the types.
    public var sortClassification : Int {
        switch self.classification {
        case .wall:
            return 0
        case .floor:
            return 1
        case .ceiling:
            return 2
        case .door:
            return 3
        case .window:
            return 4
        case .seat:
            return 5
        default:
            return 6
        }
    }
    
    public var normalVector: SIMD3<Float> {
        /**
         Returns normalized normal vector of the transform.
         */
        
        let transform = Transform(matrix: self.transform)

        // Y axis is facing up.
        let unitVector = SIMD3<Float>(0, 1, 0)
        
        // Rotate the vector by the transform rotation.
        let rotatedVector = normalize(transform.rotation.act(unitVector))
        
        return rotatedVector
    }
    
    public var origin: SIMD3<Float> {
        /**
         Returns plane anchor origin coordinates.
         */
        
        let transform = Transform(matrix: self.transform)
        return transform.translation
    }
    
    public func distanceFromPoint(point: SIMD3<Float>) -> Float {
        /**
         Returns closest distance from the point to the anchor plane.
         */
        
        // Vector from plane origin to the point.
        let vectorFromOriginToPoint = point - self.origin
        
        // Plane normal vector.
        let normal = self.normalVector
        
        // Calculate the distance by using dot product of the vectors.
        let distance = abs(simd.dot(normal, vectorFromOriginToPoint))
        
        return distance
    }
}
