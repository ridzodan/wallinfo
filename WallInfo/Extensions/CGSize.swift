//
//  CGSize.swift
//  WallInfo
//
//  Created by Dan Ridzon on 09.01.2023.
//

import Foundation

func += (lhs: inout CGSize, rhs: CGSize) {
    lhs.width += rhs.width
    lhs.height += rhs.height
}

func + (lhs: CGSize, rhs: CGSize) -> CGSize {
    .init(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
}
