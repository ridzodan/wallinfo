#  How to add new translations?

1. Product > Export Localizations > select Localization folder and save as WallInfo Localizations > confirm replace.
2. Fill missing translations in WallInfo Localizations xcloc files.
3. Product > Import Localizations > select the updated language localization files. 
