//
//  AppFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import ACKategories_iOS

final class AppFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    private weak var window: UIWindow?
    
    override func start(in window: UIWindow) {
        self.window = window
        
        prepareWindow()
        
    }
    
    private func prepareWindow() {
        let animationCoordinator = self.showStartAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            animationCoordinator.stop()
            self.showMain()
        }
        
        // Those actions will be done during the start animation.
        self.registerUserDefaults()
    }
    
    private func showStartAnimation() -> StartAnimationFlowCoordinator {
        let startAnimationCoordinator = StartAnimationFlowCoordinator()
        addChild(startAnimationCoordinator)
        let startAnimationController = startAnimationCoordinator.start()
        rootViewController = startAnimationController

        window?.rootViewController = startAnimationController
        window?.makeKeyAndVisible()
        
        return startAnimationCoordinator
    }
    
    func showMain() {
        let roomsListCoordinator = RoomsListFlowCoordinator()
        addChild(roomsListCoordinator)
        let roomsListController = roomsListCoordinator.start()
        roomsListController.tabBarItem.title = String(localized: "Rooms")
        roomsListController.tabBarItem.image = UIImage(systemName: "list.dash")
        roomsListController.tabBarItem.selectedImage = UIImage(systemName: "list.dash")
        
        let settingsCoordinator = SettingsFlowCoordinator()
        addChild(settingsCoordinator)
        let settingsController = settingsCoordinator.start()
        settingsController.tabBarItem.title = String(localized: "Settings")
        settingsController.tabBarItem.image = UIImage(systemName: "gearshape")
        settingsController.tabBarItem.selectedImage = UIImage(systemName: "gearshape.fill")
        
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [
            roomsListController,
            settingsController
        ]
        
        rootViewController = tabBarController
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }
    
    private func registerUserDefaults() {
        /**
         Registers user defaults, which will be used and sets their default value.
         */
        
        let defaults = UserDefaults.standard
        
        defaults.register(
            defaults: [
                "Display AR Info text": true,
                "ARView Crop": true,
                "AR Scale": 2.5,
                "Tracked Image Width": 100,
                "Automatically save tracker position": true,
                "Save screenshots to Photos": true
            ]
        )
    }
}
