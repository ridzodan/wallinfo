//
//  PersistenceController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 10.11.2022.
//

import CoreData

struct PersistenceController {

    // Create persistence controller singleton.
    static let shared = PersistenceController()

    let container: NSPersistentContainer

    private init() {
        // Create persistent container for this app.
        container = NSPersistentContainer(name: "WallInfo")
        
        // Tell container to load the data stack.
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    
    func saveContext() {
        
        let context = container.viewContext
        
        // Check if the container has any changes.
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError{
                print(error)
                print("Save error: \(error.localizedDescription)")
                print("Failure reason: \(error.localizedFailureReason ?? "")")
                print("Recovery suggestion: \(error.localizedRecoverySuggestion ?? "")")
            }
        }
    }
}
