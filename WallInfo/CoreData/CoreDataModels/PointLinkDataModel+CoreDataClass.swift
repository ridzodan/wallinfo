//
//  PointLinkDataModel+CoreDataClass.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData

@objc(PointLinkDataModel)
public class PointLinkDataModel: NSManagedObject {
    
    private func applyViewModelData(pointLink: PointLink) {
        
        self.rightPoint = pointLink.rightPoint.pointDataModel
        self.leftPoint = pointLink.leftPoint.pointDataModel
        self.linkType = pointLink.linkType.rawValue
    }
    
    func save(pointLink: PointLink) {
        
        self.applyViewModelData(pointLink: pointLink)
        PersistenceController.shared.saveContext()
        
        print("PointLink: xx was saved.")
    }
    
    func delete() {
        
        PersistenceController.shared.container.viewContext.delete(self)
        PersistenceController.shared.saveContext()
        
        print("PointLink: was deleted.")
    }
    
    func createViewModel(leftPoint: Point, rightPoint: Point, linkType: LinkType) -> PointLink {
        return PointLink(leftPoint: leftPoint, rightPoint: rightPoint, linkType: linkType, pointLinkDataModel: self)
    }

}
