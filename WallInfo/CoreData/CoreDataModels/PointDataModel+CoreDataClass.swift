//
//  PointDataModel+CoreDataClass.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData

@objc(PointDataModel)
public class PointDataModel: NSManagedObject {
    
    var point: Point?
    
    private func applyViewModelData(point: Point) {
        
        self.positionX = point.position.x
        self.positionY = point.position.y
        self.positionZ = point.position.z
        
        self.wall = point.wall.wallDataModel

        self.rightLinks = []
        for linkViewModel in point.rightLinks {
            guard let link = linkViewModel.pointLinkDataModel else {continue}
            self.addToRightLinks(link)
        }
        
        self.leftLinks = []
        for linkViewModel in point.leftLinks {
            guard let link = linkViewModel.pointLinkDataModel else {continue}
            self.addToLeftLinks(link)
        }
    }
    
    func save(point: Point) {
        
        self.applyViewModelData(point: point)
        PersistenceController.shared.saveContext()
        
        print("Point: [\(self.positionX), \(self.positionY), \(self.positionZ)] was saved.")
    }
    
    func delete() {
        
        PersistenceController.shared.container.viewContext.delete(self)
        PersistenceController.shared.saveContext()
        
        print("Point: [\(self.positionX), \(self.positionY), \(self.positionZ)] was deleted.")
    }
    
    func createViewModel(wall: Wall) -> Point {
        
        let position: SIMD3<Float> = SIMD3(positionX, positionY, positionZ)
        self.point = Point(position: position, wall: wall, pointDataModel: self)
        
        return self.point!
    }

}
