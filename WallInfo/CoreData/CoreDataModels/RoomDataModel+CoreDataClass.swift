//
//  RoomDataModel+CoreDataClass.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData

@objc(RoomDataModel)
public class RoomDataModel: NSManagedObject {
    
    private func applyViewModelData(room: Room) {
        self.title = room.title
        self.created = room.created

        self.walls = []
        for wall in room.walls {
            guard let wall = wall.wallDataModel else {continue}
            self.addToWalls(wall)
        }
    }
    
    func save(room: Room) {
        
        self.applyViewModelData(room: room)
        PersistenceController.shared.saveContext()
        
        print("Room: \(self.title ?? "") was saved.")
    }
    
    func delete() {
        
        PersistenceController.shared.container.viewContext.delete(self)
        PersistenceController.shared.saveContext()
        
        print("Room: \(self.title ?? "") was deleted.")
    }
    
    func createViewModel() -> Room {
        return Room(title: self.title ?? "", created: self.created ?? .now, roomDataModel: self)
    }
}
