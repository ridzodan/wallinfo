//
//  ImageDataModel+CoreDataProperties.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.11.2022.
//
//

import Foundation
import CoreData


extension ImageDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageDataModel> {
        return NSFetchRequest<ImageDataModel>(entityName: "ImageDataModel")
    }

    @NSManaged public var image: Data?
    @NSManaged public var note: String?
    @NSManaged public var created: Date?
    @NSManaged public var wall: WallDataModel?

}

extension ImageDataModel : Identifiable {

}
