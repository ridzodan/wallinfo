//
//  RoomDataModel+CoreDataProperties.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.11.2022.
//
//

import Foundation
import CoreData


extension RoomDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RoomDataModel> {
        return NSFetchRequest<RoomDataModel>(entityName: "RoomDataModel")
    }

    @NSManaged public var title: String?
    @NSManaged public var created: Date?
    @NSManaged public var walls: NSSet?

}

// MARK: Generated accessors for walls
extension RoomDataModel {

    @objc(addWallsObject:)
    @NSManaged public func addToWalls(_ value: WallDataModel)

    @objc(removeWallsObject:)
    @NSManaged public func removeFromWalls(_ value: WallDataModel)

    @objc(addWalls:)
    @NSManaged public func addToWalls(_ values: NSSet)

    @objc(removeWalls:)
    @NSManaged public func removeFromWalls(_ values: NSSet)

}

extension RoomDataModel : Identifiable {

}
