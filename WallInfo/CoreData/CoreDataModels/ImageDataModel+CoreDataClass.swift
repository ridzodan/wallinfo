//
//  ImageDataModel+CoreDataClass.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData
import UIKit

@objc(ImageDataModel)
public class ImageDataModel: NSManagedObject {
    
    func applyViewModelData(imageModel: ImageModel) {
        self.note = imageModel.note
        self.image = imageModel.image.jpegData(compressionQuality: 1.0)
        self.wall = imageModel.wall.wallDataModel
        self.created = imageModel.created
    }
    
    func save(imageModel: ImageModel) {
        
        self.applyViewModelData(imageModel: imageModel)
        PersistenceController.shared.saveContext()
        
        print("Image: \(self.id) was saved.")
    }
    
    func delete() {
        
        PersistenceController.shared.container.viewContext.delete(self)
        PersistenceController.shared.saveContext()
        
        print("Image: \(self.id) was deleted.")
    }
    
    func createViewModel(wall: Wall) -> ImageModel {
        return ImageModel(image: UIImage(data: self.image!)!, wall: wall, note: self.note ?? "", created: self.created ?? .now, imageDataModel: self)
    }
}
