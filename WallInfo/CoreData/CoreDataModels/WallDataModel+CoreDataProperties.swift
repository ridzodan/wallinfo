//
//  WallDataModel+CoreDataProperties.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.11.2022.
//
//

import Foundation
import CoreData


extension WallDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WallDataModel> {
        return NSFetchRequest<WallDataModel>(entityName: "WallDataModel")
    }

    @NSManaged public var title: String?
    @NSManaged public var note: String?
    @NSManaged public var created: Date?
    @NSManaged public var room: RoomDataModel?
    @NSManaged public var points: NSSet?
    @NSManaged public var images: NSSet?

}

// MARK: Generated accessors for points
extension WallDataModel {

    @objc(addPointsObject:)
    @NSManaged public func addToPoints(_ value: PointDataModel)

    @objc(removePointsObject:)
    @NSManaged public func removeFromPoints(_ value: PointDataModel)

    @objc(addPoints:)
    @NSManaged public func addToPoints(_ values: NSSet)

    @objc(removePoints:)
    @NSManaged public func removeFromPoints(_ values: NSSet)

}

// MARK: Generated accessors for images
extension WallDataModel {

    @objc(addImagesObject:)
    @NSManaged public func addToImages(_ value: ImageDataModel)

    @objc(removeImagesObject:)
    @NSManaged public func removeFromImages(_ value: ImageDataModel)

    @objc(addImages:)
    @NSManaged public func addToImages(_ values: NSSet)

    @objc(removeImages:)
    @NSManaged public func removeFromImages(_ values: NSSet)

}

extension WallDataModel : Identifiable {

}
