//
//  PointDataModel+CoreDataProperties.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData


extension PointDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PointDataModel> {
        return NSFetchRequest<PointDataModel>(entityName: "PointDataModel")
    }

    @NSManaged public var positionX: Float
    @NSManaged public var positionY: Float
    @NSManaged public var positionZ: Float
    @NSManaged public var wall: WallDataModel?
    @NSManaged public var rightLinks: NSSet?
    @NSManaged public var leftLinks: NSSet?

}

// MARK: Generated accessors for rightLinks
extension PointDataModel {

    @objc(addRightLinksObject:)
    @NSManaged public func addToRightLinks(_ value: PointLinkDataModel)

    @objc(removeRightLinksObject:)
    @NSManaged public func removeFromRightLinks(_ value: PointLinkDataModel)

    @objc(addRightLinks:)
    @NSManaged public func addToRightLinks(_ values: NSSet)

    @objc(removeRightLinks:)
    @NSManaged public func removeFromRightLinks(_ values: NSSet)

}

// MARK: Generated accessors for leftLinks
extension PointDataModel {

    @objc(addLeftLinksObject:)
    @NSManaged public func addToLeftLinks(_ value: PointLinkDataModel)

    @objc(removeLeftLinksObject:)
    @NSManaged public func removeFromLeftLinks(_ value: PointLinkDataModel)

    @objc(addLeftLinks:)
    @NSManaged public func addToLeftLinks(_ values: NSSet)

    @objc(removeLeftLinks:)
    @NSManaged public func removeFromLeftLinks(_ values: NSSet)

}

extension PointDataModel : Identifiable {

}
