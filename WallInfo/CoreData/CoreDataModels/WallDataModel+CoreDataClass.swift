//
//  WallDataModel+CoreDataClass.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData

@objc(WallDataModel)
public class WallDataModel: NSManagedObject {
    
    func applyViewModelData(wall: Wall) {
        self.title = wall.title
        self.room = wall.room.roomDataModel
        self.note = wall.note
        self.created = wall.created
        
        self.images = []
        for imageModel in wall.images {
            guard let image = imageModel.imageDataModel else {continue}
            self.addToImages(image)
        }
    }
    
    func save(wall: Wall) {
        self.applyViewModelData(wall: wall)
        PersistenceController.shared.saveContext()
        
        print("Wall: \(self.title ?? "") was saved.")
    }
    
    func delete() {
        PersistenceController.shared.container.viewContext.delete(self)
        PersistenceController.shared.saveContext()
        
        print("Wall: \(self.title ?? "") was deleted.")
    }
    
    func createViewModel(room: Room) -> Wall {
        return Wall(title: self.title ?? "", room: room, note: self.note, created: self.created ?? .now, wallDataModel: self)
    }

}
