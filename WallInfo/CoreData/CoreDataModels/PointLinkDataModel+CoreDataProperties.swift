//
//  PointLinkDataModel+CoreDataProperties.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//
//

import Foundation
import CoreData


extension PointLinkDataModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PointLinkDataModel> {
        return NSFetchRequest<PointLinkDataModel>(entityName: "PointLinkDataModel")
    }

    @NSManaged public var linkType: Int16
    @NSManaged public var leftPoint: PointDataModel?
    @NSManaged public var rightPoint: PointDataModel?

}

extension PointLinkDataModel : Identifiable {

}
