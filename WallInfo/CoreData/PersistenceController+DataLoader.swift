//
//  PersistenceController+DataLoader.swift
//  WallInfo
//
//  Created by Dan Ridzon on 16.11.2022.
//

import CoreData
import UIKit

extension PersistenceController {

    func getAllRooms() -> [Room] {
        /**
         Loads all the rooms and child objects from the core data and returns it as a view models.
         */
        
        // Create a fetch request.
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "RoomDataModel")
        
        // Sort descriptors.
        let createdSort = NSSortDescriptor(key:"created", ascending:true)
        request.sortDescriptors = [createdSort]
        
        // Requested objects will contain data.
        request.returnsObjectsAsFaults = false
        
        // Fetch the data.
        var roomDataModels: [RoomDataModel] = []
        do {
            roomDataModels = try container.viewContext.fetch(request) as! [RoomDataModel]
        }
        catch let error as NSError {
            print("Fetch error: \(error.localizedDescription)")
            return []
        }
        
        // Convert room data models to view models.
        let rooms = roomDataModels.map { $0.createViewModel() }

        return rooms
    }
    
    func loadWalls(room: Room) -> [Wall] {
        /**
         Loads all wall view models for specified room view model and returns them as an array.
         */

        // Get all wall data models for the room.
        guard let wallDataModels = room.roomDataModel?.walls?.allObjects as? [WallDataModel] else {return []}
        
        // Convert data models to view models.
        let walls: [Wall] = wallDataModels.map{ $0.createViewModel(room: room) }
                
        return walls.sorted(by: {$0.created < $1.created})
    }
    
    func loadImages(wall: Wall) -> [ImageModel]{
        /**
        Loads all image view models of specified wall view model and returns them as an array.
         */

        guard let imageDataModels = wall.wallDataModel?.images?.allObjects as? [ImageDataModel] else {return []}
        
        return imageDataModels.map{ $0.createViewModel(wall: wall) }.sorted(by: {$0.created < $1.created} )
    }
    
    func loadPoints(wall: Wall) -> [Point]{
        /**
         Loads all point view models of specified wall view model and returns them as an array.
         */
        
        // Get all point data madels of the wall.
        guard let pointDataModels = wall.wallDataModel?.points?.allObjects as? [PointDataModel] else {return []}
        
        // Convert point data models to view models.
        let points = pointDataModels.map { $0.createViewModel(wall: wall) }
        
        // Get all link data models of the wall points.
        var pointLinkDataModels: [PointLinkDataModel] = []
        pointDataModels.forEach {
            pointLinkDataModels.append(contentsOf: $0.rightLinks?.allObjects as? [PointLinkDataModel] ?? [])
        }

        // Load links between the points. (Links must be loaded after all the point view models are loaded as there are M-N relations.)
        loadPointLinks(pointLinkDataModels: pointLinkDataModels)

        return points
    }
    
    private func loadPointLinks(pointLinkDataModels: [PointLinkDataModel]) {
        /**
         Loads all point link view models from their data models and returns them as an array.
         */
        
        for pointLinkDataModel in pointLinkDataModels {
            
            guard
                let leftPoint = pointLinkDataModel.leftPoint?.point,
                let rightPoint = pointLinkDataModel.rightPoint?.point
            else {continue}

            let pointLink = pointLinkDataModel.createViewModel(leftPoint: leftPoint, rightPoint: rightPoint, linkType: LinkType(rawValue: pointLinkDataModel.linkType) ?? LinkType.other)
            
            leftPoint.addRightLink(link: pointLink)
            rightPoint.addLeftLink(link: pointLink)
        }
    }
}

