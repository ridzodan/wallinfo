//
//  PhotoPickerView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//

import SwiftUI
import PhotosUI
import UIKit

struct PhotoPickerView: View {
    
    var wall: Wall
    @State private var imageSelection: PhotosPickerItem?
    
    var body: some View {
        PhotosPicker(
            selection: $imageSelection,
            matching: .images,
            photoLibrary: .shared()
        ) {
            Image(uiImage: UIImage(systemName: "photo.on.rectangle")!)
                .resizable()
                .scaledToFit()
                .padding()
        }
        .onChange(of: imageSelection) { selection in
            Task {
                // Retrive selected asset in the form of Data
                if let imageData = try? await selection?.loadTransferable(type: Data.self) {
                    _ = wall.saveUIImage(image: UIImage(data: imageData)!)
                }
            }
        }
    }
}
