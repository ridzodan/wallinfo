//
//  ImageGalleryView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 18.11.2022.
//

import SwiftUI

private struct ResizableImageView: View {
    
    let image: Image
    
    @GestureState private var currentOffset: CGSize = .zero
    @State private var offset: CGSize = .zero
    
    @GestureState private var currentScale: CGFloat = 1
    @State private var  scale: CGFloat = 0
    
    var body: some View {
        image
            .resizable()
            .scaledToFit()
            .scaleEffect(scale + currentScale)
            .offset(offset + currentOffset)
            .gesture(magnificationGesture)
    }
    
    private var magnificationGesture: some Gesture {
        MagnificationGesture()
            .updating($currentScale) {value, state, _ in state = value}
            .onEnded{
                scale += $0 - 1
            }
    }
}

struct ImageGalleryView: View {
    
    @ObservedObject var wall: Wall
    @Binding var selection: UIImage
    @Binding var isPresented: Bool
    @State private var titleAlertIsPresented: Bool = false
    @State private var titleString: String = ""
    
    var body: some View {
        NavigationView {
            TabView(selection: $selection) {
                ForEach(wall.images, id: \.id) { image in
                    ResizableImageView(image: Image(uiImage: image.image))
                        .tag(image.image)
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .navigationBarItems(
                leading:
                    HStack {
                        ShareLink(item: Image(uiImage: selection), preview: SharePreview("WallInfo photo", icon: Image(uiImage: selection)))
                    },
                trailing:
                    HStack {
                        Button(action: { renameActiveImage() }, label: { Image(systemName: "pencil") })
                        Button(action: { deleteActiveImage() }, label: { Image(systemName: "trash") })
                    }
            )
            .navigationTitle(self.getActiveImageViewModel()?.note ?? "")
            .navigationBarTitleDisplayMode(.inline)
        }
        .alert("Image note", isPresented: $titleAlertIsPresented, actions: {
            TextField("Note", text: $titleString)
            Button("Save", action: { self.saveModifiedTitle() })
        })
    }
    
    private func getActiveImageViewModel() -> ImageModel? {
        /**
         Returns viewModel of image which is currently selected.
         */
        
        for imageModel in wall.images {
            guard imageModel.image == selection else { continue }
            
            return imageModel
        }
        
        return nil
    }
    
    private func deleteActiveImage() {
        /**
         Deletes image which is currently selected in the TabView. If no tabs are left, gallery sheet is closed.
         */
        
        // Get the image view model from the selection.
        let imageModel = self.getActiveImageViewModel()
        
        imageModel?.delete()
        
        // Check if it was the last image in the tabs.
        if wall.images.isEmpty {
            isPresented = false
        }
        else {
            selection = wall.images[0].image
        }
    }
    
    private func renameActiveImage() {
        /**
         Opens alert dialogue which enables user to set the note of the image.
         */
        
        let imageModel = self.getActiveImageViewModel()
        
        self.titleString = imageModel?.note ?? ""
        self.titleAlertIsPresented = true
    }
    
    private func saveModifiedTitle() {
        /**
         Saves message from title alert to image note field.
         */
        
        let imageModel = self.getActiveImageViewModel()
        imageModel?.note = self.titleString
        imageModel?.save()
    }
}
