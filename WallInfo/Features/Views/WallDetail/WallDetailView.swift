//
//  WallDetailView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 11.11.2022.
//

import SwiftUI

struct ImageButton: View {
    
    let overlayedContent: AnyView
    
    var body: some View {
        ZStack{
            Rectangle()
                .foregroundColor(.gray)
            overlayedContent
        }
        .frame(width: 80, height: 60)
        .cornerRadius(5)
        .padding(.top)
    }
}

struct WallDetailView<ViewModel: WallDetailViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: WallDetailFlowDelegate?
    
    // Because note input is multiline, separate confirm button must be used.
    @FocusState private var noteIsFocused: Bool
    
    private let columns = [
        GridItem(.adaptive(minimum: 80))
    ]
    
    var body: some View {
        List {
            Section("Note"){
                TextField("Type note here...", text: $viewModel.wall.note, axis: .vertical)
                    .lineLimit(1...5)
                    .focused($noteIsFocused)
                    .onSubmit {
                        viewModel.wall.save()
                    }
            }
            Section("Images"){
                ScrollView(.vertical){
                    LazyVGrid(columns: columns){
                        ImageButton(overlayedContent: AnyView(PhotoPickerView(wall: viewModel.wall)))
                        ImageButton(overlayedContent: AnyView(
                            SwiftUI.Image(uiImage: UIImage(systemName: "camera.on.rectangle")!)
                                .resizable()
                                .scaledToFit()
                                .padding()
                        ))
                            .onTapGesture {
                                viewModel.cameraSheetDisplayed = true
                            }
                        
                        // Display saved images.
                        ForEach(viewModel.wall.images, id: \.id) { image in
                            ImageButton(overlayedContent: AnyView(Image(uiImage: image.image).resizable().aspectRatio(contentMode: .fill)))
                                .onTapGesture {
                                    viewModel.selectedImage = image.image
                                    viewModel.gallerySheetDisplayed = true
                                }
                        }
                    }
                }
                .frame(maxHeight: 280)
            }
            Section("Wall layout"){
                ZStack{
                    Canvas{ context, size in
                    }
                    .aspectRatio(contentMode: .fit)
                    VStack{
                        Image(systemName: "arkit")
                            .resizable()
                            .frame(width: 80, height: 80)
                        Text("Tap to enter AR")
                            .font(.title2)
                            .multilineTextAlignment(.center)
                    }
                    Spacer()
                        .contentShape(Rectangle())
                        .onTapGesture {
                            flowDelegate?.showARView()
                        }
                }
            }
        }
        // Listen for the change in navigation bar title, to save the change.
        .onChange(of: $viewModel.wall.title.wrappedValue, perform: { title in
            viewModel.wall.save()
        })
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("Done") {
                    noteIsFocused = false
                    viewModel.wall.save()
                }
            }
        }
        .sheet(isPresented: $viewModel.gallerySheetDisplayed) {
            ImageGalleryView(wall: viewModel.wall, selection: $viewModel.selectedImage, isPresented: $viewModel.gallerySheetDisplayed)
                .presentationDetents([.medium, .large])
        }
        .sheet(isPresented: $viewModel.cameraSheetDisplayed, onDismiss: viewModel.saveImageFromCamera) {
            CameraImagePicker(image: $viewModel.cameraImage, isPresented: $viewModel.cameraSheetDisplayed, sourceType: .constant(.camera))
        }
    }
}

struct WallDetailView_Previews: PreviewProvider {
    static var previews: some View {
        WallDetailView(viewModel: WallDetailViewModel(wall: .dummy), flowDelegate: nil)
    }
}
