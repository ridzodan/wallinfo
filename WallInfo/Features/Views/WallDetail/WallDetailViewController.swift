//
//  WallDetailViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol WallDetailFlowDelegate: AnyObject {
    func showARView()
}

final class WallDetailViewController<ViewModel: WallDetailViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: WallDetailFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: WallDetailFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()

        let rootView = WallDetailView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = viewModel.title
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "pencil"),
            style: .plain,
            target: self,
            action: #selector(titleEditTapped)
        )
    }
    
    @objc
    private func titleEditTapped() {
        self.promptForTitleUpdate()
    }
    
    private func promptForTitleUpdate() {

        // Create alert controller.
        let ac = UIAlertController(title: String(localized: "Wall title"), message: nil, preferredStyle: .alert)
        
        // Add text input.
        ac.addTextField()
        ac.textFields![0].text = self.viewModel.title

        // Add Confirm action.
        let submitAction = UIAlertAction(title: String(localized: "Confirm"), style: .default) { [unowned ac] _ in
            guard let text = ac.textFields?[0].text, text != "" else { return }
            self.viewModel.wall.title = text
            self.viewModel.wall.save()
            self.navigationItem.title = text
        }
        ac.addAction(submitAction)
        
        // Add Cancel action.
        let cancelAction = UIAlertAction(title: String(localized: "Cancel"), style: .destructive, handler: nil)
        ac.addAction(cancelAction)

        present(ac, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.wall.loadImages()
        viewModel.wall.loadPoints()
    }
}

