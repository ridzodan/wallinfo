//
//  WallDetailViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import Combine
import SwiftUI

protocol WallDetailViewModeling: ObservableObject {

    var wall: Wall { get set }
    var gallerySheetDisplayed: Bool { get set }
    var selectedImage: UIImage { get set }
    var cameraSheetDisplayed: Bool { get set }
    var cameraImage: UIImage? { get set }

    var title: String { get }
    
    func saveImageFromCamera()
}

final class WallDetailViewModel: WallDetailViewModeling {
    
    @Published var wall: Wall
    
    // Gallery sheet
    @Published var gallerySheetDisplayed: Bool = false
    @Published var selectedImage: UIImage = UIImage()
    
    // Camera sheet
    @Published var cameraSheetDisplayed: Bool = false
    @Published var cameraImage: UIImage?

    var title: String { wall.title }
    
    var anyCancellable: AnyCancellable? = nil
    
    init(wall: Wall) {
        self.wall = wall
        
        anyCancellable = self.wall.objectWillChange.sink { [weak self] (_) in
            self?.objectWillChange.send()
        }
    }
    
    func saveImageFromCamera() {
        guard let image = cameraImage else { return }
        _ = wall.saveUIImage(image: image)
        cameraImage = nil
    }
}
