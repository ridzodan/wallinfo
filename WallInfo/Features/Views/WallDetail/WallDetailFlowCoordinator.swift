//
//  WallDetailFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import ACKategories_iOS

final class WallDetailFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    private let wall: Wall
    
    init(wall: Wall) {
        self.wall = wall
    }

    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = WallDetailViewModel(wall: wall)
        let vc = WallDetailViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension WallDetailFlowCoordinator: WallDetailFlowDelegate {
    func showARView() {
        guard let navigationController else { return }
        let wallInfoARCoordinator = WallInfoARFlowCoordinator(wall: wall)
        addChild(wallInfoARCoordinator)
        wallInfoARCoordinator.start(with: navigationController)
    }
}
