//
//  RoomDetailFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import ACKategories_iOS

final class RoomDetailFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    private let room: Room
    
    init(room: Room) {
        self.room = room
    }

    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = RoomDetailViewModel(room: room)
        let vc = RoomDetailViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension RoomDetailFlowCoordinator: RoomDetailFlowDelegate {
    func showWall(_ wall: Wall) {
        guard let navigationController else { return }
        let wallDetailCoordinator = WallDetailFlowCoordinator(wall: wall)
        addChild(wallDetailCoordinator)
        wallDetailCoordinator.start(with: navigationController)
    }
}
