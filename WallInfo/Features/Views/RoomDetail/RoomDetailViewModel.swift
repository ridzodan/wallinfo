//
//  RoomDetailViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import SwiftUI

protocol RoomDetailViewModeling: ObservableObject {
    var room: Room { get set }
    var newWallTitle: String { get set }
    var title: String { get }
    
    func addNewWall() -> Wall?
    func delete(at offsets: IndexSet)
}

final class RoomDetailViewModel: RoomDetailViewModeling {

    @Published var room: Room
    @Published var newWallTitle = ""
    var title: String { room.title }
    
    init(room: Room) {
        self.room = room
    }
    
    func addNewWall() -> Wall? {
        guard newWallTitle != "" else { return nil }
            
        // Create new wall model.
        let wall = Wall(title: newWallTitle, room: self.room, save: true)
        
        // Clear the input field.
        newWallTitle = ""
        
        return wall
    }
    
    func delete(at offsets: IndexSet){
        self.room.deleteWalls(offsets: offsets)
    }
}
