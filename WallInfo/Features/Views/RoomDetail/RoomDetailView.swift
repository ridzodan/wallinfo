//
//  RoomDetailView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 10.11.2022.
//

import SwiftUI


struct WallListInfoView: View {
    
    @ObservedObject private(set) var wall: Wall
    
    var body: some View {
        HStack{
            Text(wall.title)
                .fontWeight(.semibold)
            Spacer()
        }
        
        .padding()
        .contentShape(Rectangle())
    }
}

struct RoomDetailView<ViewModel: RoomDetailViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: RoomDetailFlowDelegate?

    var body: some View {
        List {
            ForEach(viewModel.room.walls, id: \.id) {wall in
                WallListInfoView(wall: wall)
                    .onTapGesture {
                        flowDelegate?.showWall(wall)
                    }
            }
            .onDelete(perform: viewModel.delete)
            TextField("New wall title:", text: $viewModel.newWallTitle)
                .onSubmit {
                    guard let wall = viewModel.addNewWall() else { return }
                    flowDelegate?.showWall(wall)
                }
                .padding()
        }
        // Listen for the change in navigation bar title, to save the change.
        .onChange(of: $viewModel.room.title.wrappedValue, perform: { title in
            viewModel.room.save()
        })
    }
}

struct RoomDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        RoomDetailView(viewModel: RoomDetailViewModel(room: .dummy), flowDelegate: nil)
    }
}
