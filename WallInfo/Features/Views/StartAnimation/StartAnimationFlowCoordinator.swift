//
//  StartAnimationFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import ACKategories_iOS

final class StartAnimationFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    override func start() -> UIViewController {
        
        super.start()
        
        let vm = StartAnimationViewModel()
        let vc = StartAnimationViewController(viewModel: vm, flowDelegate: self)
        let navVC = UINavigationController(rootViewController: vc)
        
        rootViewController = vc
        navigationController = navVC
        
        return navVC
    }
}

extension StartAnimationFlowCoordinator: StartAnimationFlowDelegate {
    
    func animationEnded() {

    }
}

