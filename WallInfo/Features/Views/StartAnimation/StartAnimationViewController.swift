//
//  StartAnimationViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol StartAnimationFlowDelegate: AnyObject {
    func animationEnded()
}

final class StartAnimationViewController<ViewModel: StartAnimationViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: StartAnimationFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: StartAnimationFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        let rootView = StartAnimationView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
