//
//  StartAnimationView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import SwiftUI

private func convertCoordinate(pos: CGPoint, geometry: GeometryProxy) -> CGPoint {
    let widthScale = geometry.size.width / 22
    let heightScale = geometry.size.height / 22
    
    return CGPoint(
        x: pos.x * widthScale,
        y: pos.y * heightScale
    )
}

private func getDelay(order: Double, animationSpeed: Double) -> Double {
    return animationSpeed * order
}

private struct CircleView: View {
    
    let geometry: GeometryProxy
    let order: Double
    let position: CGPoint
    let color: Color
    let animationSpeed: Double
    let animation: Animation
    let scale: Double
    
    @State private var diameter: Double = 0.0
    private let circlesScale: Double = 3/11
    
    var body: some View {
        Circle()
            .frame(width: diameter, height: diameter)
            .position(convertCoordinate(pos: position, geometry: geometry))
            .foregroundColor(color)
            .onAppear {
                withAnimation(
                    animation.delay((getDelay(order: order, animationSpeed: animationSpeed)))
                ) {
                    diameter=getCircleDiameter()
                }
            }
    }
    
    func getCircleDiameter() -> Double {
        return geometry.size.width * circlesScale * scale
    }
}

private struct LineView: View {
    
    let geometry: GeometryProxy
    let startPosition: CGPoint
    let endPosition: CGPoint
    let color: Color
    let animationSpeed: Double
    let animation: Animation
    let order: Double
    
    @State private var visiblePercentage: Double = .zero
    private let widthScale: Double = 1/11
    
    var body: some View {
        Path() { path in
            path.move(to: convertCoordinate(pos: startPosition, geometry: geometry))
            path.addLine(to: convertCoordinate(pos: endPosition, geometry: geometry))
        }
        .trim(from: 0, to: visiblePercentage)
        .stroke(color, lineWidth: getLineWidth())
        .onAppear {
            withAnimation(
                animation.delay((getDelay(order: order, animationSpeed: animationSpeed)))
            ) {
                visiblePercentage = 1.0
            }
        }
    }
    
    func getLineWidth() -> Double {
        return geometry.size.width * widthScale
    }
}

private struct LogoTextView<ViewModel: StartAnimationViewModeling>: View {
    
    @ObservedObject var viewModel: ViewModel
    let animationDuration: Double
    
    @State private var opacity: Double = 0.0
    
    var body: some View {
        Group {
            Text("Wall")
                .foregroundColor(viewModel.greyColor) +
            Text("lnfo")
                .foregroundColor(viewModel.blueColor)
        }
        .font(
            .system(size: 500)
            .weight(.bold)
        )
        .minimumScaleFactor(0.05)
        .opacity(opacity)
        .onAppear {
            withAnimation(
                .easeIn(duration: animationDuration)
            ) {
                opacity = 1.0
            }
        }
    }
}

struct StartAnimationView<ViewModel: StartAnimationViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: StartAnimationFlowDelegate?
    
    var body: some View {
        VStack {
            LogoTextView(viewModel: viewModel, animationDuration: 2)
                .padding(40)
            GeometryReader { geometry in
                ZStack {
                    ForEach(viewModel.lines) { line in
                        createLine(line: line, geometry: geometry)
                    }
                    ForEach(viewModel.circles) { circle in
                        createCircle(circle: circle, geometry: geometry)
                    }
                    ForEach(viewModel.topLines) { line in
                        createLine(line: line, geometry: geometry)
                    }
                }
            }
            .aspectRatio(1, contentMode: .fit)
            .padding()
        }
        .onAppear{
            viewModel.runHaptics()
        }
//        .task {
//            DispatchQueue.main.asyncAfter(deadline: .now() + viewModel.animationSpeed * 14) {
//                flowDelegate?.animationEnded()
//            }
//        }
    }
    
    private func createLine(line: LineData, geometry: GeometryProxy) -> LineView {
        LineView(
            geometry: geometry,
            startPosition: line.startPosition,
            endPosition: line.endPosition,
            color: line.color,
            animationSpeed: viewModel.animationSpeed,
            animation: viewModel.animation,
            order: line.order
        )
    }
    
    private func createCircle(circle: CircleData, geometry: GeometryProxy) -> CircleView {
        CircleView(
            geometry: geometry,
            order: circle.order,
            position: circle.position,
            color: circle.color,
            animationSpeed: viewModel.animationSpeed,
            animation: viewModel.animation,
            scale: circle.scale
        )
    }
}


struct StartAnimation_Previews: PreviewProvider {
    static var previews: some View {
        StartAnimationView(viewModel:  StartAnimationViewModel(), flowDelegate: nil)
    }
}
