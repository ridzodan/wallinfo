//
//  StartAnimationViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import SwiftUI

struct CircleData: Identifiable {
    let id = UUID()
    
    let order: Double
    let position: CGPoint
    let color: Color
    
    var scale: Double = 1.0
}

struct LineData: Identifiable {
    let id = UUID()
    
    let order: Double
    let startPosition: CGPoint
    let endPosition: CGPoint
    let color: Color
}

protocol StartAnimationViewModeling: ObservableObject {
    var animationSpeed: Double {get}
    var animation: Animation {get}
    var greyColor: Color {get}
    var blueColor: Color {get}
    var whiteColor: Color {get}
    
    var circles: [CircleData] {get set}
    var lines: [LineData] {get set}
    var topLines: [LineData] {get set}
    
    func runHaptics()
}

final class StartAnimationViewModel: StartAnimationViewModeling {
    let animationSpeed: Double = 0.2
    let animation = Animation.easeIn(duration: 0.4)
    
    let greyColor: Color = .init(red: 77/255, green: 77/255, blue: 77/255)
    let blueColor: Color = .init(red: 128/255, green: 179/255, blue: 255/255)
    let whiteColor: Color = Color(uiColor: UIColor.systemBackground)
    
    var circles: [CircleData] = []
    var lines: [LineData] = []
    var topLines: [LineData] = []
    
    init() {
        self.circles = [
            .init(order: 1, position: .init(x: 3, y: 3), color: greyColor),
            .init(order: 2, position: .init(x: 19, y: 3), color: greyColor),
            .init(order: 3, position: .init(x: 11, y: 19), color: whiteColor, scale: 1.2),
            .init(order: 3, position: .init(x: 11, y: 19), color: greyColor),
            .init(order: 4, position: .init(x: 3, y: 11), color: blueColor),
            .init(order: 5, position: .init(x: 3, y: 19), color: blueColor),
            .init(order: 6, position: .init(x: 19, y: 19), color: blueColor),
        ]
        
        self.lines = [
            .init(order: 7, startPosition: .init(x: 3, y: 3), endPosition: .init(x: 19, y: 3), color: greyColor),
            .init(order: 9, startPosition: .init(x: 3, y: 11), endPosition: .init(x: 3, y: 19), color: blueColor),
            .init(order: 9.5, startPosition: .init(x: 3, y: 19), endPosition: .init(x: 19, y: 19), color: blueColor),
        ]
        
        self.topLines = [
            .init(order: 8, startPosition: .init(x: 19, y: 3), endPosition: .init(x: 11, y: 19), color: greyColor),
        ]
    }
    
    func runHaptics() {
        
        // Prepare the haptic generator.
        let generator = UIImpactFeedbackGenerator(style: .soft)
        generator.prepare()
        
        // Load all the delays.
        var delays: [Double] = self.circles.map{ $0.order } //+ self.lines.map{ $0.order } + self.topLines.map{ $0.order }
        
        // Remove duplicates.
        delays = Array(Set(delays))
        
        // Create impact for each delay.
        for delay in delays {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay * self.animationSpeed + 0.4) {
                generator.impactOccurred()
            }
        }
    }
}
