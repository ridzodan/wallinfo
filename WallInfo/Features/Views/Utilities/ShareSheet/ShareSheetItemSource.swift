//
//  ShareSheetItemSource.swift
//  WallInfo
//
//  Created by Dan Ridzon on 03.04.2023.
//

import LinkPresentation
import UIKit

class ShareSheetItemSource: NSObject, UIActivityItemSource {
    
    let image: UIImage
    let title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return image
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return image
    }
    
    func activityViewControllerLinkMetadata(_ activityViewController: UIActivityViewController) -> LPLinkMetadata? {
        
        let linkMetadata = LPLinkMetadata()
        
        linkMetadata.title = title
        linkMetadata.imageProvider = NSItemProvider(object: image)
        
        return linkMetadata
    }
}
