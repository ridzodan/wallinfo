//
//  FirstSetupFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import ACKategories_iOS

final class FirstSetupFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    override func start() -> UIViewController {
        
        super.start()
        
        let vm = FirstSetupViewModel()
        let vc = FirstSetupViewController(viewModel: vm, flowDelegate: self)
        let navVC = UINavigationController(rootViewController: vc)
        
        rootViewController = vc
        navigationController = navVC
        
        return navVC
    }
    
    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = FirstSetupViewModel()
        let vc = FirstSetupViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension FirstSetupFlowCoordinator: FirstSetupFlowDelegate {
    func showCustomImage() {
        guard let navigationController else { return }
        let customImageCoordinator = CustomImageFlowCoordinator(imageSetupParentFlowCoordinator: self)
        addChild(customImageCoordinator)
        customImageCoordinator.start(with: navigationController)
    }
    
    func showPreloadedImage() {
        guard let navigationController else { return }
        let preloadedImageCoordinator = PreloadedImageFlowCoordinator(imageSetupParentFlowCoordinator: self)
        addChild(preloadedImageCoordinator)
        preloadedImageCoordinator.start(with: navigationController)
    }
}
