//
//  FirstSetupView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import SwiftUI

struct FirstSetupView<ViewModel: FirstSetupViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: FirstSetupFlowDelegate?
    
    var body: some View {
        List {
            Text(viewModel.infotext)
            Button("Use custom image"){
                flowDelegate?.showCustomImage()
            }
            Button("Use preloaded image") {
                flowDelegate?.showPreloadedImage()
            }
        }
    }
}

struct FirstSetupView_Previews: PreviewProvider {
    static var previews: some View {
        FirstSetupView(viewModel: FirstSetupViewModel(), flowDelegate: nil)
    }
}
