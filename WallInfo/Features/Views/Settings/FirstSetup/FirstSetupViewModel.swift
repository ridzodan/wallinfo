//
//  FirstSetupViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import SwiftUI

protocol FirstSetupViewModeling: ObservableObject {
    var infotext: String { get }
}

final class FirstSetupViewModel: FirstSetupViewModeling {
    
    let infotext: String = String(localized: "first-setup-infotext")
}
