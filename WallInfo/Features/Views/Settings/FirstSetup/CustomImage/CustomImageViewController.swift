//
//  CustomImageViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol CustomImageFlowDelegate: AnyObject {
    func endImageSetupFlow()
    func shareTrackingImage(image: UIImage, title: String)
}

final class CustomImageViewController<ViewModel: CustomImageViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: CustomImageFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: CustomImageFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        let rootView = CustomImageView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
