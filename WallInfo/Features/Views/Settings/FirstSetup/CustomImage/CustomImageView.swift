//
//  CustomImageView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import PhotosUI
import SwiftUI

struct CustomImagePickerView: View {
    
    @Binding var imageData: Data?
    @State private var imageSelection: PhotosPickerItem?
    
    var body: some View {
        PhotosPicker(
            selection: $imageSelection,
            matching: .images,
            photoLibrary: .shared()
        ){
            Text(imageData != nil ? "Change" : "Add photo")
        }
        .buttonStyle(.borderless)
        .onChange(of: imageSelection) { selection in
            Task {
                // Retrive selected asset in the form of Data
                if let imageData = try? await selection?.loadTransferable(type: Data.self) {
                    guard let selectedImageData = UIImage(data: imageData)?.jpegData(compressionQuality: 1) else {return}
                    self.imageData = selectedImageData
                }
            }
        }
    }
}

struct CustomImageView<ViewModel: CustomImageViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: CustomImageFlowDelegate?
    @FocusState private var widthInputIsFocused: Bool

    var body: some View {
        List {
            Text(viewModel.infotext)
            Section("Custom tracked image width") {
                HStack {
                    TextField("Width of the image", value: $viewModel.trackedImageWidth, format: .number)
                        .keyboardType(.numberPad )
                        .focused($widthInputIsFocused)
                        .foregroundColor(viewModel.widthInputColor)
                        .onTapGesture {
                            viewModel.widthInputTapped = true
                        }
                    Text("mm")
                }
            }
            Section("Custom tracked image") {
                VStack {
                    Image(uiImage: viewModel.trackedImage != nil ? UIImage(data: viewModel.trackedImage!)! : UIImage())
                        .resizable()
                        .background(Color.gray)
                        .aspectRatio(contentMode: .fit)
                    HStack {
                        CustomImagePickerView(imageData: $viewModel.trackedImage)
                        Spacer()
                        if viewModel.trackedImage != nil {
                            Button("Share") {
                                self.flowDelegate?.shareTrackingImage(image: viewModel.image, title: String(localized: "Wallinfo tracker"))
                            }
                            .buttonStyle(.borderless)
                            Button("Delete") {
                                viewModel.trackedImage = nil
                            }
                            .buttonStyle(.borderless)
                        }
                    }
                }
            }
            Button("Continue") {
                flowDelegate?.endImageSetupFlow()
            }
            .disabled(viewModel.continueDisabled)
        }
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("OK") {
                    widthInputIsFocused = false
                }
            }
        }
    }
}

struct CustomImageVieww_Previews: PreviewProvider {
    static var previews: some View {
        CustomImageView(viewModel: CustomImageViewModel(), flowDelegate: nil)
    }
}
