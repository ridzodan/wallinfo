//
//  CustomImageFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import UIKit
import ACKategories_iOS

final class CustomImageFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    weak var imageSetupParentFlowCoordinator: FirstSetupFlowCoordinator?
    
    init(imageSetupParentFlowCoordinator: FirstSetupFlowCoordinator? = nil) {
        self.imageSetupParentFlowCoordinator = imageSetupParentFlowCoordinator
    }
    
    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = CustomImageViewModel()
        let vc = CustomImageViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension CustomImageFlowCoordinator: CustomImageFlowDelegate {
    
    func endImageSetupFlow() {
        self.imageSetupParentFlowCoordinator?.stop(animated: true)
    }
    
    func shareTrackingImage(image: UIImage, title: String) {
        /**
         Displays tracking image share sheet.
         */
        
        let item = ShareSheetItemSource(image: image, title: title)
        let ac = ShareSheetController(activityItems: [item], applicationActivities: nil)
        navigationController?.present(ac, animated: true)
    }
}
