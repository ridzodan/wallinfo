//
//  CustomImageViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import SwiftUI

protocol CustomImageViewModeling: ObservableObject {
    var infotext: String { get }
    var trackedImageWidth: Double { get set }
    var trackedImage: Data? { get set }
    var image: UIImage { get }
    var widthInputTapped: Bool { get set }
    var widthInputColor: Color { get }
    var continueDisabled: Bool { get }
}

final class CustomImageViewModel: CustomImageViewModeling {
    
    let infotext: String = String(localized: "custom-image-infotext")
    var widthInputTapped: Bool = false
    
    @AppStorage("Tracked Image Width", store: .standard) var trackedImageWidth = 100.0
    
    @AppStorage("Tracked Image", store: .standard) var trackedImage: Data?
    
    var image: UIImage {
        UIImage(data: self.trackedImage ?? Data()) ?? UIImage()
    }
    
    var widthInputColor: Color {
        if widthInputTapped {
            return .primary
        }
        return .secondary
    }
    
    var continueDisabled: Bool {
        /**
         Returns true if continue button should be disabled.
         Continue is disabled if widthInput hasnt been tapped yet or image is not filled yet.
         */

        return !widthInputTapped || trackedImage == nil
    }
}
