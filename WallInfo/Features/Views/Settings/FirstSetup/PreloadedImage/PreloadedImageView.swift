//
//  PreloadedImageView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import SwiftUI

struct PreloadedImageView<ViewModel: PreloadedImageViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: PreloadedImageFlowDelegate?
    @FocusState private var widthInputIsFocused: Bool

    var body: some View {
        List {
            Text(viewModel.infotext)
            
            Section {
                VStack {
                    Image(uiImage: viewModel.image)
                        .resizable()
                        .scaledToFit()
                    HStack {
                        Spacer()
                        Button("Share") {
                            self.flowDelegate?.shareTrackingImage(image: viewModel.image, title: String(localized: "Wallinfo tracker"))
                        }
                        .buttonStyle(.borderless)
                    }
                }
            }

            Section("Printed image width.") {
                HStack {
                    TextField("Width of the image", value: $viewModel.trackedImageWidth, format: .number)
                        .keyboardType(.numberPad )
                        .focused($widthInputIsFocused)
                        .foregroundColor(viewModel.widthInputColor)
                        .onTapGesture {
                            viewModel.widthInputTapped = true
                        }
                    Text("mm")
                }
            }
            
            Section{
                Button("Continue") {
                    viewModel.saveImage()
                    flowDelegate?.endImageSetupFlow()
                }
                .disabled(viewModel.continueDisabled)
            }
        }
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                Spacer()
                Button("OK") {
                    widthInputIsFocused = false
                }
            }
        }
    }
}

struct PreloadedImageView_Previews: PreviewProvider {
    static var previews: some View {
        PreloadedImageView(viewModel: PreloadedImageViewModel(), flowDelegate: nil)
    }
}
