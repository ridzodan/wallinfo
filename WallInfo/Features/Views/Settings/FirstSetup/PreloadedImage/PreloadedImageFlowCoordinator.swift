//
//  PreloadedImageFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import UIKit
import ACKategories_iOS

final class PreloadedImageFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    weak var imageSetupParentFlowCoordinator: FirstSetupFlowCoordinator?
    
    init(imageSetupParentFlowCoordinator: FirstSetupFlowCoordinator? = nil) {
        self.imageSetupParentFlowCoordinator = imageSetupParentFlowCoordinator
    }
    
    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = PreloadedImageViewModel()
        let vc = PreloadedImageViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension PreloadedImageFlowCoordinator: PreloadedImageFlowDelegate {
    
    func endImageSetupFlow() {
        self.imageSetupParentFlowCoordinator?.stop(animated: true)
    }
    
    func shareTrackingImage(image: UIImage, title: String) {
        /**
         Displays tracking image share sheet.
         */
        
        let item = ShareSheetItemSource(image: image, title: title)
        let ac = ShareSheetController(activityItems: [item], applicationActivities: nil)
        navigationController?.present(ac, animated: true)
    }
}
