//
//  PreloadedImageViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 25.03.2023.
//

import SwiftUI

protocol PreloadedImageViewModeling: ObservableObject {
    var infotext: String { get }
    var image: UIImage { get }
    var trackedImageWidth: Double { get set }
    var widthInputTapped: Bool { get set }
    var widthInputColor: Color { get }
    var continueDisabled: Bool { get }
    
    func saveImage()
}

final class PreloadedImageViewModel: PreloadedImageViewModeling {
    
    let image: UIImage
    let infotext: String = String(localized: "preloaded-image-infotext")
    var widthInputTapped: Bool = false

    init() {
        self.image = UIImage(named: "Tracker")!
    }
    
    @AppStorage("Tracked Image Width", store: .standard) var trackedImageWidth = 100.0
    @AppStorage("Tracked Image", store: .standard) var trackedImage: Data?
    
    var widthInputColor: Color {
        if widthInputTapped {
            return .primary
        }
        return .secondary
    }
    
    func saveImage() {
        self.trackedImage = self.image.jpegData(compressionQuality: 1)
    }
    
    var continueDisabled: Bool {
        /**
         Returns true if continue button should be disabled.
         Continue is disabled if widthInput hasnt been tapped yet.
         */

        return !widthInputTapped
    }
}
