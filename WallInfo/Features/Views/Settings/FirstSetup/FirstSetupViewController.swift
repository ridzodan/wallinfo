//
//  FirstSetupViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol FirstSetupFlowDelegate: AnyObject {
    func showCustomImage()
    func showPreloadedImage()
}

final class FirstSetupViewController<ViewModel: FirstSetupViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: FirstSetupFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: FirstSetupFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        let rootView = FirstSetupView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
