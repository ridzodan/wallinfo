//
//  SettingsViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol SettingsFlowDelegate: AnyObject {
    func showImageTrackerSetup()
}

final class SettingsViewController<ViewModel: SettingsViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: SettingsFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: SettingsFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        let rootView = SettingsView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = String(localized: "settings-title")
        navigationItem.largeTitleDisplayMode = .automatic
    }
}
