//
//  SettingsViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import SwiftUI

protocol SettingsViewModeling: ObservableObject {
    var arScale: Double { get set }
    var cropARView: Bool { get set }
    var displayHelp: Bool { get set }
    var automaticallySaveTrackerPosition: Bool { get set }
    var saveScreenshotsToPhotos: Bool { get set }
}

final class SettingsViewModel: SettingsViewModeling {
    
    @AppStorage("AR Scale", store: .standard) var arScale = 2.5
    @AppStorage("ARView Crop", store: .standard) var cropARView = true
    @AppStorage("Display AR Info text", store: .standard) var displayHelp = true
    @AppStorage("Automatically save tracker position", store: .standard) var automaticallySaveTrackerPosition = true
    @AppStorage("Save screenshots to Photos", store: .standard) var saveScreenshotsToPhotos = true
}
