//
//  SettingsFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import ACKategories_iOS

final class SettingsFlowCoordinator: Base.FlowCoordinatorNoDeepLink {

    override func start() -> UIViewController {
        
        super.start()
        
        let vm = SettingsViewModel()
        let vc = SettingsViewController(viewModel: vm, flowDelegate: self)
        let navVC = UINavigationController(rootViewController: vc)
        
        rootViewController = vc
        navigationController = navVC
        
        return navVC
    }
}

extension SettingsFlowCoordinator: SettingsFlowDelegate {
    func showImageTrackerSetup() {
        guard let navigationController else { return }
        let firstSetupCoordinator = FirstSetupFlowCoordinator()
        addChild(firstSetupCoordinator)
        firstSetupCoordinator.start(with: navigationController)
    }
}
