//
//  SettingsView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 07.01.2023.
//

import SwiftUI

struct SettingsView<ViewModel: SettingsViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: SettingsFlowDelegate?
    
    var body: some View {
        List {
            Section("AR Settings") {
                Text("AR Scale")
                Slider(
                    value: $viewModel.arScale,
                    in: 1.0...4.0,
                    minimumValueLabel: Image(systemName: "minus.magnifyingglass"),
                    maximumValueLabel: Image(systemName: "plus.magnifyingglass"),
                    label: {EmptyView()}
                )
                Toggle(isOn: $viewModel.cropARView) {
                    Text("Crop ARView to fill the screen")
                }
                Toggle(isOn: $viewModel.displayHelp) {
                    Text("Display help")
                }
                Toggle(isOn: $viewModel.automaticallySaveTrackerPosition) {
                    Text("Automatically save tracker position")
                }
                Toggle(isOn: $viewModel.saveScreenshotsToPhotos) {
                    Text("Save screenshots to Photos")
                }
            }
            Section("Image tracker") {
                Button("ImageTrackerSetup") {
                    flowDelegate?.showImageTrackerSetup()
                }
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(viewModel: SettingsViewModel(), flowDelegate: nil)
    }
}
