//
//  ARAction.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import Foundation

enum ARAction {
    case placePoint(linkType: LinkType)
    case deleteLastPoint
    case createSnapshot
    case setSnapping(snappingOn: Bool)
}
