//
//  ARActionManager.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import Combine


class ARActionManager {
    static let shared = ARActionManager()
    private init() {}
    
    var actionStream = PassthroughSubject<ARAction, Never>()
}
