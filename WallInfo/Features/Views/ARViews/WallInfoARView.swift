//
//  WallInfoARView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import SwiftUI

private struct ARViewHelpOverlay: View {
    
    private struct HelpLine: View {
        
        let icon: String
        let text: String
        
        var body: some View {
            HStack{
                Image(systemName: icon)
                    .resizable()
                    .frame(width: 25, height: 25)
                    .padding(2)
                Text(text)
                    .multilineTextAlignment(.leading)
                Spacer()
            }
                .padding(.bottom)
        }
    }
    
    @Binding var isDisplayed: Bool
    
    var body: some View {
        if isDisplayed{
            GeometryReader { geometry in
                ZStack {
                    RoundedRectangle(cornerRadius: 12)
                        .frame(width: geometry.size.width - 50, height: geometry.size.height - 50)
                        .foregroundColor(.gray)
                    VStack {
                        HelpLine(icon: "", text: String(localized: "Tap on point to select it. Tap elsewhere to deselect a point.", table: "Help"))
                        HelpLine(icon: "arrow.up.and.down.square", text: String(localized: "Select a type of link.", table: "Help"))
                        HelpLine(icon: "plus.square", text: String(localized: "Adds new point to the layout. If active point is selected, link between them will be created (of selected type).", table: "Help"))
                        HelpLine(icon: "trash.square", text: String(localized: "Deletes selected point.", table: "Help"))
                        HelpLine(icon: "point.topleft.down.curvedto.point.bottomright.up.fill", text: String(localized: "Point snapping is turned on.", table: "Help"))
                        HelpLine(icon: "point.topleft.down.curvedto.point.bottomright.up", text: String(localized: "Point snapping is turned off.", table: "Help"))
                        HelpLine(icon: "camera.viewfinder", text: String(localized: "Creates a screenshot of the scene.", table: "Help"))
                    }
                    .frame(width: geometry.size.width - 70, height: geometry.size.height - 50)
                }
                .onTapGesture {
                    self.isDisplayed = false
                }
                .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
            }
        }
    }
}

private struct ARActionButton: View {
    
    @State var action: () -> Void
    let icon: String
    var scale: CGFloat = 1
    
    var body: some View {
        Button {
            let buttonImpact = UIImpactFeedbackGenerator(style: .soft)
            buttonImpact.impactOccurred()
            action()
        } label: {
            Image(systemName: icon)
                .resizable()
                .scaledToFit()
                .frame(width: 30 * scale, height: 30 * scale)
                .padding()
                .background(.regularMaterial)
                .cornerRadius(12)
                .opacity(0.80)
        }
    }
}

private struct LinkTypePickerView: View {
    
    @Binding var selectedLinkType: LinkType
    
    var body: some View {
        Menu {
            Picker(selection: $selectedLinkType) {
                ForEach(LinkType.allCases) { linkType in
                    HStack {
                        Text("\(linkType.getName())")
                        Image(systemName: linkType.getSymbol())
                    }
                    .tag(linkType)
                }
            } label: { }
        } label: {
            HStack {
                Image(systemName: "arrow.up.and.down.square")
                    .resizable()
                    .scaledToFit()
                Spacer()
                Image(systemName: selectedLinkType.getSymbol())
                    .resizable()
                    .scaledToFit()
            }
        }
        .frame(width: 100, height: 30)
        .padding()
        .background(.regularMaterial)
        .cornerRadius(12)
        .opacity(0.80)
    }
}

private struct ARViewActionButtons<ViewModel: WallInfoARViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    
    var body: some View {
        VStack{
            Spacer()
            HStack {
                LinkTypePickerView(selectedLinkType: $viewModel.selectedLinkType)
                Spacer()
                ARActionButton(action: {ARActionManager.shared.actionStream.send(.placePoint(linkType: viewModel.selectedLinkType))}, icon: "plus.square", scale: 1.5)
                Spacer()
                ARActionButton(action: {ARActionManager.shared.actionStream.send(.deleteLastPoint)}, icon: "trash.square")
                ARActionButton(
                    action: {
                        self.viewModel.snappingOn = !self.viewModel.snappingOn
                        ARActionManager.shared.actionStream.send(.setSnapping(snappingOn: self.viewModel.snappingOn))
                    },
                    icon: self.viewModel.snappingOn ? "point.topleft.down.curvedto.point.bottomright.up.fill" : "point.topleft.down.curvedto.point.bottomright.up"
                )
            }
            .padding()
        }
        .overlay {
            ARViewHelpOverlay(isDisplayed: self.$viewModel.showHelp)
        }
    }
}

struct WallInfoARView<ViewModel: WallInfoARViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: WallInfoARFlowDelegate?
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                // ARView.
                #if !targetEnvironment(simulator)
                
                // If crop is off use the given aspect ratio.
                if !viewModel.cropARView {
                    ImageTrackingARViewControllerRepresentable(viewModel: viewModel.createImageTrackingViewModel())
                        .aspectRatio(viewModel.aspectRatio, contentMode: .fit)
                }
                
                // If crop is on.
                else {
                    ImageTrackingARViewControllerRepresentable(viewModel: viewModel.createImageTrackingViewModel())
                }
                #endif
                
                // ARView center marker.
                Circle()
                    .foregroundColor(.secondary)
                    .frame(width: 10, height: 10)
                    .position(x: geometry.size.width / 2, y: geometry.size.height / 2)
                
                // ARView action butons.
                ARViewActionButtons(viewModel: viewModel)
                
                // Display info message popup, when there is a message to be displayed.
                if viewModel.infoMessage != "" {
                    infoMessagePopup
                        .position(x: geometry.size.width / 2, y: 100)
                }
            }
            .onAppear{
                viewModel.updateARViewAspectRatio(geometry: geometry)
            }
        }
        .alert(self.viewModel.missingImageTrackerMessage, isPresented: $viewModel.displayMissingImageTrackerWarning) {
            Button("OK", role: .cancel) {
                flowDelegate?.showImageTrackerSettings()
            }
        }
        .alert(
            self.viewModel.arInfoText, isPresented: $viewModel.displayARInfoText) {
                Button("OK") {}
                Button("Don't show again") {
                    let defaults = UserDefaults.standard
                    defaults.set(false, forKey: "Display AR Info text")
                }
            }
    }
    
    private var infoMessagePopup: some View {
        Text(viewModel.infoMessage)
            .padding()
            .font(.title2)
            .multilineTextAlignment(.center)
            .foregroundColor(.black)
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .foregroundColor(.init(hue: 0, saturation: 0, brightness: 0.9))
                    .opacity(0.9)
            )
            .padding()
            .onTapGesture {
                viewModel.infoMessage = ""
            }
    }
}

struct WallInfoARView_Previews: PreviewProvider {
    
    static var previews: some View {
        WallInfoARView(viewModel: WallInfoARViewModel(wall: .dummy), flowDelegate: nil)
    }
}
