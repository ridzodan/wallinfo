//
//  ImageTrackingARView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import ARKit
import RealityKit
import SnapKit

class ImageTrackingARView: ARView, ARCoachingOverlayViewDelegate {
    typealias ViewModel = ImageTrackingViewModeling
    
    var viewModel: ViewModel!
    
    required dynamic init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required init(frame: CGRect) {
        
        super.init(frame: frame)

        // Create AR session configuration.
        let configuration = ARWorldTrackingConfiguration()
        
        // Turn off environment texturing.
        configuration.environmentTexturing = .none
        
        self.debugOptions = []
        
        // Load the reference images.
        let referenceImages = self.getReferenceImages()

        // Add reference images to the configuration.
        configuration.detectionImages = referenceImages
        configuration.maximumNumberOfTrackedImages = 1
        
        // Disable screen dimming.
        UIApplication.shared.isIdleTimerDisabled = true
        
        // Setup the plane tracking configuration.
        configuration.planeDetection = [.vertical, .horizontal]

        // Run the session configuration.
        self.session.run(configuration)
        
        // Enable the coaching overlay.
        self.addCoaching()
    }
    
    // MARK: - Coaching overlay.
    func addCoaching() {

        #if !targetEnvironment(simulator)
        let coachingOverlay = ARCoachingOverlayView()
        coachingOverlay.delegate = self
        coachingOverlay.session = self.session
        coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        coachingOverlay.goal = .verticalPlane
        self.addSubview(coachingOverlay)
        
        coachingOverlay.snp.makeConstraints{ make in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.width.equalTo(0)
            make.height.equalTo(0)
        }
        #endif
    }
    
    // MARK: - Load reference image
    private func getReferenceImages() -> Set<ARReferenceImage> {
        /**
         Loads reference images, which will be used for image tracking.
         If user set up custom image in settings, this picture is used, otherwise default from assets is used.
         */
        
        // First try to load reference image from user defaults.
        let defaults = UserDefaults.standard
        let imageData = defaults.data(forKey: "Tracked Image")
        let imageWidth = defaults.float(forKey: "Tracked Image Width")
        
        if imageData != nil, imageWidth > 0, let image = UIImage(data: imageData!)?.cgImage {
            let referenceImage = ARReferenceImage(image, orientation: .up, physicalWidth: CGFloat(imageWidth) / 1000) // User sets the width of image in mm, but arKit uses m.
            referenceImage.name = "Custom image."
            return [referenceImage]
        }
            
       return []
    }
    
    public func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
        print("Coaching overlay did deactivated.")
    }
}

//MARK: - AR scene rendering methods.
extension ImageTrackingARView {

    func renderPoint(point: Point, referenceAnchorEntity: AnchorEntity) {
        /**
         This method renders entity on the screen for the given point view model.
         */
        
        let boxSize: Float = 0.02 * self.viewModel.arScale
        let collisionScale: Float = 1.4
        let cornerRadius = 0.001 * self.viewModel.arScale
        
        // Add point cube to the scene.
        let materials = [SimpleMaterial(color: .gray, roughness: 0.8, isMetallic: true)]
        let box = ModelEntity(mesh: MeshResource.generateBox(size: boxSize, cornerRadius: cornerRadius), materials: materials)
        referenceAnchorEntity.addChild(box, preservingWorldTransform: false)
        box.setPosition(point.position, relativeTo: referenceAnchorEntity)
        viewModel.addPointRenderedEntity(point: point, entity: box)
        
        // Add collision group 1 to the box.
        box.collision = CollisionComponent(
            shapes: [.generateBox(width: boxSize * collisionScale, height: boxSize * collisionScale, depth: boxSize * collisionScale)],
            mode: .default,
            filter: CollisionFilter(group: CollisionGroup(rawValue: 1 << 1), mask: .default)
        )
    }
    
    func renderLink(link: PointLink, referenceAnchorEntity: AnchorEntity, renderLinkIcon: Bool = true) {
        /**
         This method renders link entity on the screen for the given link view model.
         If renderLinkIcon attribute is true, link symbol icon is also rendered.
         */
        
        let start = link.leftPoint
        let end = link.rightPoint
        
        // Get distance from start to end.
        let pointsDistance = distance(start.position, end.position)
        
        // Get link type width. (Very small random value is added, so crossing lines of same width arent interfering with eachother).
        let width: Float = link.linkType.getWidth() * self.viewModel.arScale + Float.random(in: 0 ..< 0.00001)
        
        // Box depth is equal to distance between the points.
        let rectangleEntity = ModelEntity(mesh: .generateBox(width: width, height: width, depth: pointsDistance), materials: [link.linkType.getMaterial()])
        
        // Get middle between the two points - box will be placed there.
        let middle = SIMD3<Float>((start.position.x + end.position.x)/2, (start.position.y + end.position.y)/2, (start.position.z + end.position.z)/2)
        
        rectangleEntity.setPosition(middle, relativeTo: referenceAnchorEntity)
        rectangleEntity.look(at: start.position, from: middle, relativeTo: nil)
        referenceAnchorEntity.addChild(rectangleEntity, preservingWorldTransform: false)
        viewModel.addLinkRenderedEntity(link: link, entity: rectangleEntity)
        
        if renderLinkIcon {
            self.renderLinkIcon(link: link, referenceAnchorEntity: referenceAnchorEntity)
        }
    }
    
    func renderLinkIcon(link: PointLink, referenceAnchorEntity: AnchorEntity) {
        /**
         Renders link icon in the middle of the link.
         */
        
        let radius: Float = 0.01 * self.viewModel.arScale + Float.random(in: 0 ..< 0.00001)
        let height: Float = link.linkType.getWidth() * self.viewModel.arScale + 0.00002
        
        // Get middle between the link - icon will be placed here.
        let middle = link.getLinkMiddle()
        
        // Create entity at the middle of the link, which will be parent entity for all entities placed at the middle of the link.
        let middleEntity = Entity()
        referenceAnchorEntity.addChild(middleEntity, preservingWorldTransform: false)
        middleEntity.setPosition(middle, relativeTo: referenceAnchorEntity)
        viewModel.setLinkMiddleEntity(link: link, entity: middleEntity)
        
        // Render the circle.
        var mesh: MeshResource!
        do {
            mesh = try MeshResource.generateCylinder(radius: radius, height: height, smoothNormals: true)
        } catch {
            mesh = MeshResource.generateBox(size: radius * 2)
        }
        let cylinderEntity = ModelEntity(mesh: mesh, materials: [link.linkType.getMaterial()])
        
        // Set position of the cylinder entity.
        middleEntity.addChild(cylinderEntity, preservingWorldTransform: false)
        cylinderEntity.setPosition(.zero, relativeTo: middleEntity)
        
        // Add link symbol.
        let symbolPosition = SIMD3<Float>(0, height / 2 + 0.001, 0)
        guard let symbolEntity = viewModel.getLinkSymbolEntity(link: link) else { return }
        
        // Set position of the link symbol entity.
        middleEntity.addChild(symbolEntity, preservingWorldTransform: false)
        symbolEntity.setPosition(symbolPosition, relativeTo: middleEntity)
        let symbolScale = self.viewModel.arScale * 2
        symbolEntity.setScale(SIMD3<Float>(symbolScale, symbolScale, symbolScale), relativeTo: middleEntity)
    }
    
    func renderActivePointMarker(selectedPoint: Point?, referenceAnchorEntity: AnchorEntity) {
        /**
         Method makes sure, that after new point addition / point deletion active point marker is located on the right coordinates.
         */
        
        guard let selectedPoint = selectedPoint else {
            // If no point is selected, move helper outside the screen
            self.viewModel.activePoint?.setPosition(SIMD3(x: -200, y: -200, z: 0), relativeTo: referenceAnchorEntity)
            return
        }
        
        // When active point marker is not yet in the scene, generate its entities.
        if self.viewModel.activePoint == nil {
            let activePoint = Entity()
            referenceAnchorEntity.addChild(activePoint, preservingWorldTransform: false)
            
            let size: Float = 0.02 * self.viewModel.arScale
            let height: Float = 0.001
            let materials = [UnlitMaterial(color: .orange)]
            
            let p1 = ModelEntity(mesh: MeshResource.generateBox(width: size, height: height, depth: size * 4), materials: materials)
            activePoint.addChild(p1, preservingWorldTransform: false)
            p1.setPosition(SIMD3(x: -size * 1.5, y: 0, z: 0), relativeTo: activePoint)
            
            let p2 = ModelEntity(mesh: MeshResource.generateBox(width: size, height: height, depth: size * 4), materials: materials)
            activePoint.addChild(p2, preservingWorldTransform: false)
            p2.setPosition(SIMD3(x: size * 1.5, y: 0, z: 0), relativeTo: activePoint)
            
            let p3 = ModelEntity(mesh: MeshResource.generateBox(width: size * 4, height: height, depth: size), materials: materials)
            activePoint.addChild(p3, preservingWorldTransform: false)
            p3.setPosition(SIMD3(x: 0, y: 0, z: -size * 1.5), relativeTo: activePoint)
            
            let p4 = ModelEntity(mesh: MeshResource.generateBox(width: size * 4, height: height, depth: size), materials: materials)
            activePoint.addChild(p4, preservingWorldTransform: false)
            p4.setPosition(SIMD3(x: 0, y: 0, z: size * 1.5), relativeTo: activePoint)
            
            self.viewModel.activePoint = activePoint
        }
        
        // Update position of the active point marker.
        self.viewModel.activePoint?.setPosition(selectedPoint.position, relativeTo: referenceAnchorEntity)
    }
}
