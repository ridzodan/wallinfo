//
//  ImageTrackingARViewController+ARSessionDelegate.swift
//  WallInfo
//
//  Created by Dan Ridzon on 31.12.2022.
//

import ARKit
import RealityKit

extension ImageTrackingARViewController: ARSessionDelegate {
    
    func getTrackedImageAnchor(detectedAnchors: [ARAnchor]) -> ARImageAnchor? {
        /**
         Returns Image anchor if is found inside the detected anchors.
         */
        
        let imageAnchors = detectedAnchors.filter({ $0 is ARImageAnchor }) as! [ARImageAnchor]
        return imageAnchors.first
    }
    
    func getTransformNormalVector(_ transform: Transform) -> SIMD3<Float> {
        /**
         Returns normal vector of the transform
         */

        // Y axis is facing up.
        let unitVector = SIMD3<Float>(0, 1, 0)
        
        // Rotate the vector by the transform rotation.
        let rotatedVector = normalize(transform.rotation.act(unitVector))
        
        return rotatedVector
    }
    
    func angleBetweenVectors(_ vector1: SIMD3<Float>, _ vector2: SIMD3<Float>) -> Float {
        /**
         Returns radian angle between two SIMD3 vectors
         */
        
        // Create vector dot product.
        let dotProduct = dot(vector1, vector2)
        
        // Get cos angle between the vectors.
        let cosAngle = dotProduct / (length(vector1) * length(vector2))
        
        // Convert the cos value to radians using the acos function.
        let angle = acos(cosAngle)

        return angle
    }
    
    func getDetectedPlaneAnchor(detectedAnchors: [ARAnchor], imageTransform: Transform?) -> ARPlaneAnchor? {
        /**
         Returns plane anchor which is facing the same direction as the image anchor..
         */
        
        // Maximum angle which is accepted as similiar to the image tracker.
        let ANGLE_TRESHOLD: Float = 0.5
        
        // Filter just anchors, which are ARPlaneAnchor type.
        let planeAnchors = detectedAnchors.filter({ $0 is ARPlaneAnchor }) as! [ARPlaneAnchor]
        
        // Filter anchors, which normal angle to reference image normal is in suitable error range.
        if let imageTransform = imageTransform {
            
            let imageNormalVector = getTransformNormalVector(imageTransform)

            // First planes are filtered by their angle (Only planes facing the direction as the ref. image are accepted.)
            // After that the plane, which is the closest to the point is selected.
            return planeAnchors
                .filter({ angleBetweenVectors(imageNormalVector, getTransformNormalVector(Transform(matrix: $0.transform))) < ANGLE_TRESHOLD })
                .sorted(by: { first, second in
                    let firstDistance = first.distanceFromPoint(point: imageTransform.translation)
                    let secondDistance = second.distanceFromPoint(point: imageTransform.translation)
                    return firstDistance < secondDistance
                })
                .first
        }
        
        return nil
    }

    // This method is called, when anchor(s) have been detected.
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]){

        #if !targetEnvironment(simulator)

        // If tracked image anchor is found for the first time, create a invisible plane to the scene, to the same position as the image.
        if self.lastReferenceImageTransform == nil, let imageAnchor = getTrackedImageAnchor(detectedAnchors: anchors) {
            // MARK: - This code will run after the image tracker is detected in the scene for the first time.
            let referenceImage = imageAnchor.referenceImage

            // Generate plane overlaying the imageAnchor.
            let plane = MeshResource.generatePlane(width: Float(referenceImage.physicalSize.width), depth: Float(referenceImage.physicalSize.height))
            let planeEntity = ModelEntity(mesh: plane, materials: [SimpleMaterial(color: UIColor(hex: 0xdede64).withAlphaComponent(0.90), isMetallic: false)])

            // Add the plane to the scene.
            let anchor = AnchorEntity(anchor: imageAnchor)
            anchor.addChild(planeEntity)
            self.arView.scene.anchors.append(anchor)

            // Copy image anchor transform to another anochor. (Because stuff connected to image anchor dissappear, when image anchor is not visible)
            self.referenceAnchorEntity.transform.matrix = imageAnchor.transform
            self.arView.scene.anchors.append(self.referenceAnchorEntity)

            // Generate invisible plane, which will be used for raycasting points from screen on the wall.
            let raycastPlane = MeshResource.generatePlane(width: 20, depth: 20) // 20m x 20m
            let invisibleMaterial = UnlitMaterial(color: UIColor(white: 0, alpha: -1))
            self.raycastPlaneEntity = ModelEntity(mesh: raycastPlane, materials: [invisibleMaterial])
            self.raycastPlaneEntity!.generateCollisionShapes(recursive: true)
            self.referenceAnchorEntity.addChild(self.raycastPlaneEntity!)
            
            // Copy last seen transform of the reference image.
            self.lastReferenceImageTransform = Transform(matrix: imageAnchor.transform)
            
            // Display message that image tracker was detected.
            self.displayImageWasDetectedInfo()
            
            // Create scene screenshot.
            if viewModel.saveScreenshotAfterInitialImageDetection {
                self.createScreenSnapshot(notifyUser: false, imageTitle: .init(localized: "Image tracker position (automatically saved)."), saveToPhotos: false)
            }
            
            // Check 15s after initial image detection if wall behind the image has been loaded.
            DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
                self.checkWallDetection()
            }
        }
        #endif
    }

    // This method is called, when anchor(s) did update.
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        
        #if !targetEnvironment(simulator)
        
        // Detect if image anchor has been udpated.
        if let imageAnchor = getTrackedImageAnchor(detectedAnchors: anchors), imageAnchor.isTracked {
            
            // Copy last seen transform of the reference image.
            self.lastReferenceImageTransform = Transform(matrix: imageAnchor.transform)
        }
        
        // Detect if wall anchor has been updated.
        if let wallAnchor = getDetectedPlaneAnchor(detectedAnchors: anchors, imageTransform: self.lastReferenceImageTransform) {
            
            // MARK: - Actions which are run, when first wall is detected.
            if self.lastWallDetected == nil {
                                
                // Display wall was detected info popup.
                self.displayWallWasDetectedInfo()

                // Render saved wall data.
                self.renderSavedData()
            }
            
            // Save last seen wall anchor.
            self.lastWallDetected = wallAnchor
        }
        
        // Continue only if both wall and reference image have been already detected.
        guard let imageTransform = self.lastReferenceImageTransform, let wallAnchor = self.lastWallDetected else { return }
        
        // Create copy of reference anchor entity transform, which will be used for calculating new position and rotation.
        var updatedTransform = self.referenceAnchorEntity.transform

        // Update the transform with translation of the image imageAnchor.
        updatedTransform.translation = imageTransform.translation
        
        // Create temporary anchor entity for further calculations.
        let tmpAnchorEntity = AnchorEntity(anchor: wallAnchor)
        self.arView.scene.anchors.append(tmpAnchorEntity)
        
        // Rotation of the reference entity is copied from the detected wall, as it is more porecise then the tracked image.
        updatedTransform.rotation = simd_quatf(wallAnchor.transform)
        
        // Convert origin of reference entity to coordinate system relative to the wall origin.
        updatedTransform = tmpAnchorEntity.convert(transform: updatedTransform, from: nil)
        
        // Set the y position (relative to the wall anchor) of the reference entity to 0, so it is flush with the wall.
        updatedTransform.translation.y = 0
        
        // Apply the changed Transform to the reference entity.
        self.referenceAnchorEntity.setTransformMatrix(updatedTransform.matrix, relativeTo: tmpAnchorEntity)
        
        // Remove the temporary anchor entity.
        tmpAnchorEntity.removeFromParent()

        #endif
    }

    // This method is called after each frame update.
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        
        #if !targetEnvironment(simulator)

        // If point snapping is turned on, raycast is tested on the center of the screen, if there is already any point, which will be used when creating new link instead of creating new point.
        if self.pointSnapping {

            // Get middle of the screen.
            let center = CGPoint(x: self.arView.frame.width / 2, y: self.arView.frame.height / 2)

            // Raycast from camera to the plane and check for raycast group 1.
            guard let ray = self.arView.ray(through: center) else { return }
            let raycastResults = self.arView.scene.raycast(origin: ray.origin, direction: ray.direction, length: 10, mask: CollisionGroup(rawValue: 1 << 1))

            // If nothing was hit, reset last hit.
            if raycastResults.isEmpty {
                self.lastRaycastHit = nil
            }

            else {
                let result = raycastResults[0].entity
                guard result != self.lastRaycastHit else {return}

                self.lastRaycastHit = result

                // If entity is hit for the first time, run haptics.
                let impactGenerator = UIImpactFeedbackGenerator(style: .rigid)
                impactGenerator.impactOccurred()
            }
        }
        
        #endif
    }
}
