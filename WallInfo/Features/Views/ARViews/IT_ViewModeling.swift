//
//  ImageTrackingViewModeling.swift
//  WallInfo
//
//  Created by Dan Ridzon on 15.03.2023.
//

import Combine
import RealityKit
import UIKit

protocol ImageTrackingViewModeling {
    
    var arScale: Float { get }
    
    var activePoint: Entity? { get set }
    
    var infoMessagePasstgrough: PassthroughSubject<String, Never> { get }
    
    var saveScreenshotAfterInitialImageDetection: Bool { get }
    
    var saveScreenshotToPhotos: Bool { get }
    
    // MARK: - Methods accessed by the ViewController.

    func getPointFromEntity(entity: Entity?) -> Point?
    
    func createPoint(position: SIMD3<Float>) -> Point
    
    func addPointToWall(point: Point)
    
    func createLink(from: Point, to: Point, linkType: LinkType) -> PointLink
    
    func deletePoint(point: Point)
    
    func saveImage(image: UIImage, imageTitle: String?)
    
    func getPoints() -> [Point]
    
    func linkExists(pointA: Point?, pointB: Point?) -> Bool
    
    func createInfoMessageClearTask(task: Task<(), any Error>?)
    
    func cancelInfoMessageClearTask()
    
    // MARK: - Methods accessed by the View.
    
    func addPointRenderedEntity(point: Point, entity: Entity)
    
    func addLinkRenderedEntity(link: PointLink, entity: Entity)
    
    func setLinkMiddleEntity(link: PointLink, entity: Entity)
    
    func getLinkSymbolEntity(link: PointLink) -> Entity?
}
