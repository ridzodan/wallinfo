//
//  WallInfoARViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol WallInfoARFlowDelegate: AnyObject {
    func showImageTrackerSettings()
}

final class WallInfoARViewController<ViewModel: WallInfoARViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: WallInfoARFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: WallInfoARFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()

        let rootView = WallInfoARView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(
                image: UIImage(systemName: "questionmark.square.dashed"),
                style: .plain,
                target: self,
                action: #selector(openHelpTapped)
            ),
            UIBarButtonItem(
                image: UIImage(systemName: "camera.viewfinder"),
                style: .plain,
                target: self,
                action: #selector(createScreenshotTapped)
            ),
        ]
    }
    
    @objc
    private func openHelpTapped() {
        let buttonImpact = UIImpactFeedbackGenerator(style: .rigid)
        buttonImpact.impactOccurred()
        self.viewModel.showHelp = !self.viewModel.showHelp
    }
    
    @objc
    private func createScreenshotTapped() {
        let buttonImpact = UIImpactFeedbackGenerator(style: .rigid)
        buttonImpact.impactOccurred()
        ARActionManager.shared.actionStream.send(.createSnapshot)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Check if user have set up the image tracker.
        if !checkImageTrackerSetup() {
            self.viewModel.displayMissingImageTrackerWarning = true
        }
        
        // Hide the tab bar.
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the tab bar.
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func checkImageTrackerSetup() -> Bool {
        /**
         Returns false if image tracker is missing.
         */
        let defaults = UserDefaults.standard
        let imageData = defaults.data(forKey: "Tracked Image")
        let imageWidth = defaults.float(forKey: "Tracked Image Width")
        
        if imageData != nil, imageWidth > 0, let _ = UIImage(data: imageData!)?.cgImage {
            return true
        }
        return false
    }
    
}
