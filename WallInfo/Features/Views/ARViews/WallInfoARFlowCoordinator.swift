//
//  WallInfoARFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import UIKit
import ACKategories_iOS

final class WallInfoARFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    
    private let wall: Wall
    
    init(wall: Wall) {
        self.wall = wall
    }

    override func start(with navigationController: UINavigationController) {
        
        super.start(with: navigationController)
        
        let vm = WallInfoARViewModel(wall: wall)
        let vc = WallInfoARViewController(viewModel: vm, flowDelegate: self)
        navigationController.pushViewController(vc, animated: true)
        
        rootViewController = vc
    }
}

extension WallInfoARFlowCoordinator: WallInfoARFlowDelegate {
    
    func showImageTrackerSettings() {
        self.stop()
        
        guard let navigationController else { return }
        let firstSetupCoordinator = FirstSetupFlowCoordinator()
        addChild(firstSetupCoordinator)
        firstSetupCoordinator.start(with: navigationController)
    }
}

final class testFlowCoordinator: Base.FlowCoordinator<AppFlowCoordinator> {
    
}
