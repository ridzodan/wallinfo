//
//  ImageTrackingARViewControllerRepresentable.swift
//  WallInfo
//
//  Created by Dan Ridzon on 31.12.2022.
//

import SwiftUI


struct ImageTrackingARViewControllerRepresentable: UIViewControllerRepresentable {

    let viewModel: ImageTrackingViewModel

    func makeUIViewController(context: Context) -> ImageTrackingARViewController {
        
        return ImageTrackingARViewController(viewModel: viewModel)
    }
    
    func updateUIViewController(_ uiViewController: ImageTrackingARViewController, context: Context) {
        
    }
}
