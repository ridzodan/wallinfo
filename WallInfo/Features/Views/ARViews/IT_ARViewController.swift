//
//  ImageTrackingARViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 31.12.2022.
//

import ARKit
import Combine
import RealityGeometries
import RealityKit
import SwiftUI
import UIKit

class ImageTrackingARViewController: UIViewController {
    typealias ViewModel = ImageTrackingViewModeling
    
    let viewModel: ViewModel
    var arView: ImageTrackingARView!
    
    init(viewModel: ViewModel) {

        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    // This initializer won't be used as this app doesn't use storyboards.
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("This class does not support NSCoder.")
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Create the ARView.
        self.arView = ImageTrackingARView(frame: self.view.frame)
        
        // Set the controller as the delegate of the arView.
        self.arView.session.delegate = self
        
        // Pass the viewModel to the view.
        self.arView.viewModel = viewModel
        
        // Initialize tap gesture handler.
        self.arView.setupGestures()
        
        // Allow flexible size of the view.
        self.arView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Add the ARView to the view.
        self.view.addSubview(arView)
        
        // Subscribe to ARAction stream,
        self.subscribeToActionStream()
        
        // Check after 15 seconds if image tracker was detected in the scene.
        DispatchQueue.main.asyncAfter(deadline: .now() + 15) {
            self.checkImageTrackerDetection()
        }
    }
    
    // MARK: - Controller properties
    
    // This entity is the reference entity, which defines the coordinate system of the wall.
    var referenceAnchorEntity: AnchorEntity = AnchorEntity()
    
    // This entity contains plane mesh, which should be placed right at the wall in the real space.
    // It is used during raycasting to get coordinate relative to the referenceAnchorEntity from coordinate on the screen.
    var raycastPlaneEntity: ModelEntity?
    
    // This property contains entity, which is currently in the center of the screen.
    var lastRaycastHit: Entity?
    
    // This property contains point, which is currently selected by the user.
    var selectedPoint: Point? = nil
    
    // If this property is true, existing point (which is user aiming at) will be reused instead of making a new one.
    var pointSnapping: Bool = true
    
    // This property contains last known state of the reference image anchor transform.
    var lastReferenceImageTransform: Transform?
    
    // This property contains last detected wall anchor.
    var lastWallDetected: ARPlaneAnchor?
    
    
    // MARK: - Action stream handler.
    private var cancellables: Set<AnyCancellable> = []
    
    private func subscribeToActionStream() {
        ARActionManager.shared
            .actionStream
            .sink { [weak self] action in
                switch action {
                case .placePoint(let linkType):
                    self?.addPoint(linkType: linkType)
                case .deleteLastPoint:
                    self?.deleteSelectedPoint()
                case .createSnapshot:
                    self?.createScreenSnapshot(saveToPhotos: self?.viewModel.saveScreenshotToPhotos ?? false)
                case .setSnapping(let snappingOn):
                    self?.setPointSnapping(snappingOn: snappingOn)
                }
            }
            .store(in: &cancellables)
    }
    
    private func getTapRealPosition(tapPoint: CGPoint) -> SIMD3<Float>? {
        /**
         Raycasts tap on the screen to the screen and returns SIMD3 position of the raycasterd hit in the scene.
         Raycast plane is used as the hit object on the screen.
         */
        
        guard let plane = self.raycastPlaneEntity else { return nil }
        
        // Raycast from camera to the plane.
        guard let ray = self.arView.ray(through: tapPoint) else { return nil}
        let raycastResults = self.arView.scene.raycast(origin: ray.origin, direction: ray.direction, length: 10)
        
        // Iterate the results and check if target entity is the raycastPlaneEntity
        for result in raycastResults {
            guard result.entity == plane else {continue}
            
            // Create temporary entity, which is used to calculate the offset from the ref entity.
            let tmpEntity = Entity()
            tmpEntity.setPosition(result.position, relativeTo: nil)
            
            // Calculate the position.
            return tmpEntity.position(relativeTo: self.referenceAnchorEntity)
        }
        return nil
    }
    
    private func addPoint(disconnect: Bool = false, linkType: LinkType) {
        /**
         This method is called, when user wants to add new point to the schema.
         */
        
        var newPoint: Point? = nil
        
        // If snap (magnet) point is available.
        if self.pointSnapping && self.lastRaycastHit != nil {
            newPoint = self.viewModel.getPointFromEntity(entity: self.lastRaycastHit)
            
            // If snap point is the same as selected point -> don't create the link.
            if self.selectedPoint != nil && newPoint == self.selectedPoint {
                return
            }
            
            // If link between the selected point and snap point already exists -> don't create the link.
            if self.viewModel.linkExists(pointA: self.selectedPoint, pointB: newPoint) {
                
                // Move active point marker to the other point.
                self.selectedPoint = newPoint
                self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
                
                return
            }
        }
        
        // If no point is selected by snap, or snapping is off - create new point.
        if newPoint == nil {

            // Get middle of the screen.
            let center = CGPoint(x: self.arView.frame.width / 2, y: self.arView.frame.height / 2)
            
            // Raycast the real world position from the center of the screen position.
            guard let position = self.getTapRealPosition(tapPoint: center) else { return }

            // Create new point view Model.
            newPoint = self.viewModel.createPoint(position: position)

            // Render created point.
            self.arView.renderPoint(point: newPoint!, referenceAnchorEntity: self.referenceAnchorEntity)
        }
        
        guard let newPoint = newPoint else { return }
        self.viewModel.addPointToWall(point: newPoint)
        
        // Create link to the prev point.
        if !disconnect && self.selectedPoint != nil {
            
            // Create link view model.
            let linkViewModel = self.viewModel.createLink(from: newPoint, to: self.selectedPoint!, linkType: linkType)
            
            // Render created link.
            self.arView.renderLink(link: linkViewModel, referenceAnchorEntity: self.referenceAnchorEntity)
        }
        
        // Set new point as active point and move the indicator.
        self.selectedPoint = newPoint
        self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
    }
    
    private func deleteSelectedPoint(){
        /**
         If active point is selected. It is deleted with all connected links.
         */

        guard let point = self.selectedPoint else {return}
        
        // If point was connected just to one other point. Active point moves to the other point.
        if point.getAllLinks().count == 1 {
            self.selectedPoint = point.getAllLinks()[0].getOtherPoint(point: point)
        }
        else {
            self.selectedPoint = nil
        }
        self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
        
        // Delete point from the persistant storage.
        self.viewModel.deletePoint(point: point)
    }
    
    func createScreenSnapshot(notifyUser: Bool = true, imageTitle: String? = nil, saveToPhotos: Bool = true) {
        /**
         Generates a screenshot of the current ARView state and saves it to the wall image gallery.
         If notifyUser is set to true - popup with info about screenshot will be displayed.
         Optionally custom image title can be set.
         */

        self.arView.snapshot(saveToHDR: false) { image in
            guard let image: UIImage = image else { return }
            self.viewModel.saveImage(image: image, imageTitle: imageTitle)
            
            
            // Save image to the phone Photos album.
            if saveToPhotos {
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            }
            
            // Display message to the user, that screenshot was successful.
            if notifyUser {
                self.displayScreenshotWasTakenInfo()
            }
        }
    }
    
    func renderSavedData() {
        /**
         Renders saved points and links to the screen.
         */
        
        // Iterate all the points in the wall.
        for point in self.viewModel.getPoints() {
            
            // Render point.
            self.arView.renderPoint(point: point, referenceAnchorEntity: self.referenceAnchorEntity)
            
            // Iterate just right links, so links are not duplicates.
            for link in point.rightLinks {
                self.arView.renderLink(link: link, referenceAnchorEntity: self.referenceAnchorEntity)
            }
        }
        
        // Show active point marker.
        self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
    }
    
    func raycastActivePoint(tapLocation: CGPoint) -> Point? {
        /**
         Checks if there is a point cube under users finger. If yes returns its Point
         */
        
        // Raycast from tap location and check for hits in raycast group 1.
        guard let ray = self.arView.ray(through: tapLocation) else {return nil}
        let raycastResults = self.arView.scene.raycast(origin: ray.origin, direction: ray.direction, length: 10, mask: CollisionGroup(rawValue: 1 << 1))
        
        // If nothing was hit, set active point to nil.
        if raycastResults.isEmpty {
            let impactGenerator = UIImpactFeedbackGenerator(style: .soft)
            impactGenerator.impactOccurred()
            return nil
        }
        else {
            
            // Check which point has been hit and set it as active point.
            let impactGenerator = UIImpactFeedbackGenerator(style: .rigid)
            impactGenerator.impactOccurred()
            let entity = raycastResults[0].entity
            return self.viewModel.getPointFromEntity(entity: entity)
        }
    }
    
    func selectPoint(tapLocation: CGPoint) {
        /**
         When user taps on the screen, raycast is done to check if any of the points has been tapped.
         If point is tapped, it gets selected and active point marker moves to it.
         If tapped coordinate is empty previous active point is deselected and active point marker dissappears.
         */
        
        // Raycast the scene for the point.
        self.selectedPoint = self.raycastActivePoint(tapLocation: tapLocation)
        
        // Update active point marker location.
        self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
    }
    
    func setPointSnapping(snappingOn: Bool){
        self.pointSnapping = snappingOn
    }
    
    func movePoint(fingerPosition: CGPoint, panState: UIGestureRecognizer.State?) {
        /**
         If finger is dragger over the screen and point cube is under the finger.
         Point is moved with the finger and at the end of the pan saved.
         */
        
        // If pan continues but no point is selected - exit (nothing has to be moved).
        if panState != .began && selectedPoint == nil { return }
        
        switch panState {
        case .began:
            // At the start of the pan, check if there is any point under the finger.
            guard let point = self.raycastActivePoint(tapLocation: fingerPosition) else {
                self.selectedPoint = nil
                self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
                return
            }
            self.selectedPoint = point
            
            // Update selected point location to the tap location.
            guard let position = self.getTapRealPosition(tapPoint: fingerPosition) else { return }
            self.selectedPoint?.updatePosition(position: position)
            self.updatePointEntities(point: self.selectedPoint!)
            
            self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)
            
        case .changed:
            // Update selected point location to the tap location.
            guard let position = self.getTapRealPosition(tapPoint: fingerPosition) else { return }
            self.selectedPoint?.updatePosition(position: position)
            self.updatePointEntities(point: self.selectedPoint!)
            
            self.arView.renderActivePointMarker(selectedPoint: self.selectedPoint, referenceAnchorEntity: self.referenceAnchorEntity)

        case .ended:
            // Save point position to the persistent memory.
            self.selectedPoint?.save()

        default:
            break
        }
    }
    
    func updatePointEntities(point: Point, updateConnectedLinks: Bool = true) {
        /**
         Updates point entitities (including link entities).
         (When point is moved - this method will updates the rendered entities so they are on the correct location.)
         */
        
        for entity in point.renderedEntities {
            entity.setPosition(point.position, relativeTo: self.referenceAnchorEntity)
        }

        // Link entities cannot be easily moved, as the link changes length and angle, when point is moved. So link entity is recreated from scratch. (Cylinder and icon at the middle is just moved).
        if updateConnectedLinks {
            for link in point.getAllLinks() {
                link.deleteRenderedEntities()
                self.arView.renderLink(link: link, referenceAnchorEntity: self.referenceAnchorEntity, renderLinkIcon: false)
                link.linkMiddleEntity?.setPosition(link.getLinkMiddle(), relativeTo: self.referenceAnchorEntity)
            }
        }
    }
    
    // MARK: - infoMessagePasstgrough methods.
    
    func clearInfoPopup() {
        /**
         Sends empty string to the passthrough, so the popup disappears.
         */
        
        self.viewModel.infoMessagePasstgrough.send("")
    }
    
    func displayMessage(text: String, displayTime: Double? = nil, hapticNotification: UINotificationFeedbackGenerator.FeedbackType? = nil) {
        /**
         Displays info message popup on the screen with supplied text content.
         If displayTime is filled the text is after that delay removed, otherwise it stays on the screen forever.
         If noti
         */
        
        // If clear message task is running - cancel it, so it does not clear this message.
        self.viewModel.cancelInfoMessageClearTask()
        
        self.viewModel.infoMessagePasstgrough.send(text)
        
        // Play haptics.
        if let hapticNotification = hapticNotification {
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(hapticNotification)
        }
        
        // Check if message has time limit.
        guard let displayTime = displayTime else { return }
            
        // Create new task for clearing the message.
        let clearMessageTask = Task { [weak self] in
            try await Task.sleep(for: .seconds(displayTime))
            self?.clearInfoPopup()
        }
        self.viewModel.createInfoMessageClearTask(task: clearMessageTask)
    }

    func checkImageTrackerDetection() {
        /**
         If image tracker was not yet detected in the scene, help message is sent through the passthrough to the swiftUI popup.
         */
        
        if self.lastReferenceImageTransform == nil {
            displayMessage(text: String(localized: "image-tracker-not-yet-detected-message"), hapticNotification: .warning)
        }
    }
    
    func checkWallDetection(){
        /**
         If correct wall was not yet detected in the scene, help message is sent through the passthrough to the swiftUI popup.
         */
        
        if self.lastWallDetected == nil {
            displayMessage(text: String(localized: "wall-not-yet-detected-message"), hapticNotification: .warning)
        }        
    }
    
    func displayImageWasDetectedInfo() {
        /**
         Displays info message, that image tracker has been detected.
         Popup will disappear after 5 seconds.
         */
        
        displayMessage(text: String(localized: "image-tracker-was-detected-message"), displayTime: 5, hapticNotification: .success)
    }
    
    func displayWallWasDetectedInfo() {
        /**
         Displays info message, that wall behind the image tracker has been detected.
         Popup will disappear after 5 seconds.
         */
        
        displayMessage(text: String(localized: "wall-was-detected-message"), displayTime: 5, hapticNotification: .success)
    }
    
    func displayScreenshotWasTakenInfo() {
        /**
         Displays info message, that screenshot of AR scene was taken.
         This message disappears after 2 seconds.
         */
        
        displayMessage(text: String(localized: "ar-screenshot-taken-message"), displayTime: 2)
    }
}
