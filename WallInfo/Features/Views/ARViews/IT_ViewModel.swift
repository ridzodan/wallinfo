//
//  ImageTrackingViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 31.12.2022.
//

import Combine
import RealityKit
import UIKit

class ImageTrackingViewModel: ImageTrackingViewModeling {
    
    // This property stores the scale of the AR elements.
    private(set) var arScale: Float

    // This is the parent entity of the marker, which visualizes the currently selected point.
    var activePoint: Entity? = nil
    
    let infoMessagePasstgrough: PassthroughSubject<String, Never>
    private var clearInfoMessageTask: Task<(), any Error>?

    private let wall: Wall
    
    required init(wall: Wall, arScale: Float = 1, infoMessagePasstgrough: PassthroughSubject<String, Never>) {
        self.wall = wall
        self.arScale = arScale
        self.infoMessagePasstgrough = infoMessagePasstgrough
    }
    
    var saveScreenshotAfterInitialImageDetection: Bool {
        /**
         Checks whether user wants to create screenshot of the scene after the tracking image is initially detected.
         */
        
        if UserDefaults.standard.object(forKey: "Automatically save tracker position") != nil {
            return UserDefaults.standard.bool(forKey: "Automatically save tracker position")
        }
        return true
    }
    
    var saveScreenshotToPhotos: Bool {
        /**
         Checks whether user wants to save screenshot of the scene also in the phone Photos album.
         */
        
        if UserDefaults.standard.object(forKey: "Save screenshots to Photos") != nil {
            return UserDefaults.standard.bool(forKey: "Save screenshots to Photos")
        }
        return true
    }
    
    // MARK: - Methods accessed by the ViewController.
    
    func getPointFromEntity(entity: Entity?) -> Point? {
        return self.wall.getPointFromChildEntity(entity: entity)
    }
    
    func createPoint(position: SIMD3<Float>) -> Point {
        return Point(position: position, wall: self.wall, save: true)
    }
    
    func addPointToWall(point: Point) {
        self.wall.addPoint(point: point)
    }
    
    func createLink(from: Point, to: Point, linkType: LinkType) -> PointLink {
        return from.createLink(otherPoint: to, linkType: linkType, save: true)
    }
    
    func deletePoint(point: Point) {
        point.destruct(save: true)
    }
    
    func saveImage(image: UIImage, imageTitle: String? = nil) {
        
        // If no title is supplied - current datetime is set as title of the image.
        let title: String = imageTitle ?? DateFormatter.localizedString(from: Date(), dateStyle: .medium, timeStyle: .short)
        
        let imageModel = ImageModel(image: image, wall: self.wall, note: title, save: true)
        self.wall.addImage(image: imageModel)
    }
    
    func getPoints() -> [Point] {
        return self.wall.points
    }
    
    func linkExists(pointA: Point?, pointB: Point?) -> Bool {
        /**
         Returns true if link between point A and B already exists. (Doesn't matter on the link orientation).
         */
        
        if pointA == nil || pointB == nil {
            return false
        }
        
        for link in pointA!.leftLinks {
            if link.leftPoint == pointB {
                return true
            }
        }
        
        for link in pointA!.rightLinks {
            if link.rightPoint == pointB {
                return true
            }
        }
        
        return false
    }
    
    func createInfoMessageClearTask(task: Task<(), any Error>?) {
        self.clearInfoMessageTask = task
    }
    
    func cancelInfoMessageClearTask() {
        self.clearInfoMessageTask?.cancel()
    }
    
    // MARK: - Methods accessed by the View.
    
    func addPointRenderedEntity(point: Point, entity: Entity) {
        point.addRenderedEntity(entity: entity)
    }
    
    func addLinkRenderedEntity(link: PointLink, entity: Entity) {
        link.addRenderedEntity(entity: entity)
    }
    
    func setLinkMiddleEntity(link: PointLink, entity: Entity) {
        link.setMiddleEntity(entity: entity)
    }
    
    func getLinkSymbolEntity(link: PointLink) -> Entity? {
        return link.getLinkSymbolEntity()
    }
}
