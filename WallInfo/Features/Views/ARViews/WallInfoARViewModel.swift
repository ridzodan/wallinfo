//
//  WallInfoARViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 24.03.2023.
//

import SwiftUI
import Combine

protocol WallInfoARViewModeling: ObservableObject {
    var wall: Wall { get set }
    var aspectRatio: Double { get set}
    var cropARView: Bool { get }
    
    var displayMissingImageTrackerWarning: Bool { get set }
    var missingImageTrackerMessage: String { get }
    
    var displayARInfoText: Bool { get set }
    var arInfoText: String { get }
    
    var selectedLinkType: LinkType { get set }
    var snappingOn: Bool { get set }
    var showHelp: Bool { get set }
    
    var infoMessage: String { get set }
    
    func updateARViewAspectRatio(geometry: GeometryProxy)
    func createImageTrackingViewModel() -> ImageTrackingViewModel
}

final class WallInfoARViewModel: WallInfoARViewModeling {

    @Published var wall: Wall
    @Published var aspectRatio: Double = 3/4
    @Published var displayMissingImageTrackerWarning: Bool = false
    @Published var displayARInfoText: Bool = true
    @AppStorage("ARView Crop", store: .standard) var cropARView = true
    
    @Published var selectedLinkType: LinkType = .wire
    @Published var snappingOn: Bool = true
    @Published var showHelp: Bool = false
    
    @Published var infoMessage: String = ""
    
    let missingImageTrackerMessage: String = String(localized: "missing-image-tracker-text")
    let arInfoText: String = String(localized: "ar-info-text")
    
    // Passthrough subject for tranfering messages from UIKit ARView.
    private let passthroughSubject = PassthroughSubject<String, Never>()
    private var cancellables = Set<AnyCancellable>()
    
    init(wall: Wall) {
        self.wall = wall
        
        // Check if AR info text should be displayed, when view is displayed.
        if UserDefaults.standard.object(forKey: "Display AR Info text") != nil {
            displayARInfoText = UserDefaults.standard.bool(forKey: "Display AR Info text")
        }
        
        self.setupPassthroughSubject()
    }
    
    private func setupPassthroughSubject() {
        /**
         This methods will sink on the passthrough subject and when message arrives, it is written inside the infoMessage property.
         */
        
        self.passthroughSubject
            .sink { [weak self] text in
                self?.infoMessage = text
            }
            .store(in: &cancellables)
    }
    
    func updateARViewAspectRatio(geometry: GeometryProxy) {
        /**
         Returns 3/4 if device is in portrait orientation and 4/3 if device is in landscape orientation.
         */

        if geometry.size.width / geometry.size.height < 1 {
            self.aspectRatio = 3/4
        }
        else {
            self.aspectRatio = 4/3
        }
    }
    
    func createImageTrackingViewModel() -> ImageTrackingViewModel {
        
        var arScale: Float = 1
        
        // Load the AR scale value from the user defaults.
        if UserDefaults.standard.object(forKey: "AR Scale") != nil {
            arScale = UserDefaults.standard.float(forKey: "AR Scale")
        }
        
        return ImageTrackingViewModel(wall: wall, arScale: arScale, infoMessagePasstgrough: passthroughSubject)
    }
}

