//
//  ImageTrackingARView+TapHandler.swift
//  WallInfo
//
//  Created by Dan Ridzon on 27.12.2022.
//

import UIKit
import RealityKit

extension ImageTrackingARView {

    func setupGestures() {
        /**
         Registers tap gesture recognizer.
         */

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        
        self.addGestureRecognizer(tap)
        self.addGestureRecognizer(pan)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        /**
         Method is called, when AR View tap gesture happens.
         */

        guard let touchInView = sender?.location(in: self) else { return }
        let controller = self.session.delegate as? ImageTrackingARViewController
        controller?.selectPoint(tapLocation: touchInView)
    }
    
    @objc func handlePan(_ sender: UIPanGestureRecognizer? = nil) {
        /**
         Method is called, when AR View pan gesture happens.
         */
        
        guard let panLocation = sender?.location(in: self) else { return }
        let controller = self.session.delegate as? ImageTrackingARViewController
        controller?.movePoint(fingerPosition: panLocation, panState: sender?.state)
    }
}
