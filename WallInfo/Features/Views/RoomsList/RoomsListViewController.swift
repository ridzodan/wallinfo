//
//  RoomsListViewController.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import SwiftUI
import ACKategories_iOS

protocol RoomsListFlowDelegate: AnyObject {
    func showRoom(_ room: Room)
    func showImageSetup()
}

final class RoomsListViewController<ViewModel: RoomsListViewModeling>: Base.ViewController {
    private let viewModel: ViewModel
    weak var flowDelegate: RoomsListFlowDelegate?
    
    // MARK: - Initialization
    
    init(
        viewModel: ViewModel,
        flowDelegate: RoomsListFlowDelegate? = nil
    ) {
        self.viewModel = viewModel
        self.flowDelegate = flowDelegate
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        super.loadView()
        
        let rootView = RoomsListView(viewModel: self.viewModel, flowDelegate: flowDelegate)
        let vc = UIHostingController(rootView: rootView)
        embedController(vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = String(localized: "rooms-list-title")
        navigationItem.largeTitleDisplayMode = .automatic
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel.loadRooms()
    }
    
    override func viewWillFirstAppear(_ animated: Bool) {
    
        super.viewWillFirstAppear(animated)
        
        if !checkImageTracker() {
            flowDelegate?.showImageSetup()
        }
    }
    
    private func checkImageTracker() -> Bool {
        /**
         Checks if user have configured custom image tracker in the user defaults.
         */
        
        let defaults = UserDefaults.standard
        let imageData = defaults.data(forKey: "Tracked Image")
        let imageWidth = defaults.float(forKey: "Tracked Image Width")
        
        if imageData == nil || imageWidth <= 0 {
            return false
        }
        return true
    }
}
