//
//  RoomsListViewModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 10.11.2022.
//

import SwiftUI

protocol RoomsListViewModeling: ObservableObject {

    var rooms: [Room] { get }
    var newRoomTitle: String { get set }
    
    func loadRooms()
    func addRoom() -> Room?
    func delete(offsets: IndexSet)
}

final class RoomsListViewModel: RoomsListViewModeling {
    
    @Published private(set) var rooms: [Room] = []
    @Published var newRoomTitle = ""
    
    func loadRooms() {
        /**
        Loads all rooms data from the persistance memory.
         */
        
        // To prevent unwanted reloads, it is checked if the data has been already loaded.
        if self.rooms == [] {
            self.rooms = PersistenceController.shared.getAllRooms()
        }
    }
    
    func addRoom() -> Room? {
        if newRoomTitle != "" {
            
            // Create room model.
            let room = Room(title: newRoomTitle, save: true)
            
            // Update the room list.
            rooms.append(room)
            
            // Clear the input field
            newRoomTitle = ""

            return room
        }
        return nil
    }
    
    func delete(offsets: IndexSet) {
        
        // Delete all indexes from the data stack.
        for offset in offsets {
            self.rooms[offset].delete()
        }
        
        // Delete view models from the list.
        self.rooms.remove(atOffsets: offsets)
    }
}
