//
//  RoomsListView.swift
//  WallInfo
//
//  Created by Dan Ridzon on 10.11.2022.
//

import SwiftUI

private struct RoomsInfoView: View {
    
    @ObservedObject private(set) var room: Room
    
    var body: some View {
        HStack {
            Text(room.title)
                .fontWeight(.semibold)
            Spacer()
        }
        .padding()
        .contentShape(Rectangle())
    }
}

struct RoomsListView<ViewModel: RoomsListViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
    let flowDelegate: RoomsListFlowDelegate?
    
    var body: some View {
        List {
            ForEach(viewModel.rooms, id: \.id) {room in
                RoomsInfoView(room: room)
                    .onTapGesture {
                        flowDelegate?.showRoom(room)
                    }
            }
            .onDelete(perform: viewModel.delete)
            TextField("New room title:", text: $viewModel.newRoomTitle)
                .onSubmit {
                    guard let room = viewModel.addRoom() else { return }
                    flowDelegate?.showRoom(room)
                }
                .padding()
        }
    }
}

struct RoomsListView_Previews: PreviewProvider {
    static var previews: some View {
        RoomsListView(viewModel: RoomsListViewModel(), flowDelegate: nil)
            .environment(\.locale, .init(identifier: "cs"))
    }
}

