//
//  RoomsListFlowCoordinator.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import UIKit
import ACKategories_iOS

final class RoomsListFlowCoordinator: Base.FlowCoordinatorNoDeepLink {
    override func start() -> UIViewController {
        
        super.start()
        
        let vm = RoomsListViewModel()
        let vc = RoomsListViewController(viewModel: vm, flowDelegate: self)
        let navVC = UINavigationController(rootViewController: vc)
        
        rootViewController = vc
        navigationController = navVC
        
        return navVC
    }
}

extension RoomsListFlowCoordinator: RoomsListFlowDelegate {
    
    func showRoom(_ room: Room) {
        guard let navigationController else { return }
        let roomDetailCoordinator = RoomDetailFlowCoordinator(room: room)
        addChild(roomDetailCoordinator)
        roomDetailCoordinator.start(with: navigationController)
    }
    
    func showImageSetup() {
        let firstSetupCoordinator = FirstSetupFlowCoordinator()
        addChild(firstSetupCoordinator)
        let vc = firstSetupCoordinator.start()
        vc.modalPresentationStyle = .fullScreen
        vc.isModalInPresentation = true
        
        self.navigationController?.present(vc, animated: true)
    }
}
