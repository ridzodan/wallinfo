//
//  ImageModel.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import Foundation
import UIKit

class ImageModel: ObservableObject, Identifiable {
    
    var imageDataModel: ImageDataModel?
    @Published var note: String
    var image: UIImage
    let created: Date
    var wall: Wall
    let id: UUID = UUID()
    
    init(
            image: UIImage,
            wall: Wall,
            note: String = "",
            created: Date = .now,
            imageDataModel: ImageDataModel? = nil,
            save: Bool = false
    ) {
        self.image = image
        self.wall = wall
        self.note = note
        self.created = created
        self.imageDataModel = imageDataModel
        if save {
            self.save()
        }
    }
    
}


// MARK: Core data inteface
extension ImageModel {
    
    func save() {
        
        // Create core data entity if not exists.
        if self.imageDataModel == nil {
            self.imageDataModel = ImageDataModel(context: PersistenceController.shared.container.viewContext)
        }
        
        self.imageDataModel!.save(imageModel: self)
    }
    
    func delete() {
        self.imageDataModel?.delete()
        self.wall.deleteImage(image: self, save: true)
    }
}


// MARK: Equatable extension
extension ImageModel: Equatable {
    static func == (lhs: ImageModel, rhs: ImageModel) -> Bool {
        return lhs.id == rhs.id
    }
}
