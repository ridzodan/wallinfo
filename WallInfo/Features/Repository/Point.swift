//
//  Point.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import Foundation
import RealityKit

class Point: ObservableObject, Identifiable {
    
    private(set) var position: SIMD3<Float> // Position relative to the reference entity (image marker)
    private(set) var leftLinks: [PointLink] = []
    private(set) var rightLinks: [PointLink] = []
    let wall : Wall
    let id: UUID = UUID()

    private(set) var renderedEntities: [Entity] = []
    
    var pointDataModel: PointDataModel?
    
    init(
            position: SIMD3<Float>,
            wall: Wall,
            pointDataModel: PointDataModel? = nil,
            save: Bool = false
    ) {
        self.position = position
        self.position.y = 0
        self.wall = wall

        self.pointDataModel = pointDataModel
        
        if save {
            self.save()
        }
    }
    
    func destruct(save: Bool = false){
        /**
         This method removes all PointLinks connected to this point and also removes all linked objects on the screen.
         */
        
        // Delete own entities from the screen
        self.deleteFromScene()
        
        // Itarate all linked links and delete them also.
        for link in self.getAllLinks() {
            link.destruct(save: save)
        }
        
        // Remove wall reference to this point.
        self.wall.deletePointReference(point: self)
        
        if save {
            self.delete()
        }
    }
    
    deinit {
        self.destruct()
    }
    
    func updatePosition(position: SIMD3<Float>, save: Bool = false) {
        self.position = position
        if save {
            self.save()
        }
    }
    
    func getAllLinks() -> [PointLink] {
        /**
         Returns both left and right links combined.
         */
        
        return self.rightLinks + self.leftLinks
    }
}


// MARK: RealityKit methods
extension Point {
    
    func deleteFromScene() {

        // Remove all rendered entities from the screen.
        for entity in self.renderedEntities {
            entity.removeFromParent()
        }
        
        self.renderedEntities = []
    }
    
    func addRenderedEntity(entity: Entity){
        guard !self.renderedEntities.contains(entity) else {return}
        self.renderedEntities.append(entity)
    }
}


// MARK: Links methods
extension Point {
    
    func createLink(otherPoint: Point, linkType: LinkType, save: Bool = false) -> PointLink{
        
        // Create new link view model.
        let link = PointLink(leftPoint: self, rightPoint: otherPoint, linkType: linkType, save: save)
        
        // Save link to both entities.
        self.addRightLink(link: link)
        otherPoint.addLeftLink(link: link)
        
        return link
    }
    
    func deleteLeftLink(link: PointLink) {
        guard let index = self.leftLinks.firstIndex(of: link) else {return}
        self.leftLinks.remove(at: index)
    }
    
    func deleteRightLink(link: PointLink) {
        guard let index = self.rightLinks.firstIndex(of: link) else {return}
        self.rightLinks.remove(at: index)
    }
    
    func addLeftLink(link: PointLink) {
        self.leftLinks.append(link)
    }
    
    func addRightLink(link: PointLink) {
        self.rightLinks.append(link)
    }
}


// MARK: Equatable implementation
extension Point: Equatable{
    
    static func == (lhs: Point, rhs: Point) -> Bool {
        return lhs.id == rhs.id
    }
}


// MARK: Core data inteface
extension Point {
    
    func save() {
        
        // Create core data entity if not exists.
        if self.pointDataModel == nil {
            self.pointDataModel = PointDataModel(context: PersistenceController.shared.container.viewContext)
        }
        
        self.pointDataModel!.save(point: self)
    }
    
    func delete() {
        
        self.pointDataModel?.delete()
    }
}
