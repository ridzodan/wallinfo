//
//  LinkType.swift
//  WallInfo
//
//  Created by Dan Ridzon on 17.11.2022.
//

import Foundation
import RealityKit
import UIKit

enum LinkType: Int16 {
    case wire = 0
    case pipe = 1
    case interet = 2
    case gas = 3
    case heating = 4
    case other = 5
    case wallBorder = 6
    
    func getName() -> String {
        switch self {
        case .wire:
            return String(localized: "Wire", table: "LinkTypes")
        case .pipe:
            return String(localized: "Pipe", table: "LinkTypes")
        case .interet:
            return String(localized: "Internet", table: "LinkTypes")
        case .gas:
            return String(localized: "Gas", table: "LinkTypes")
        case .heating:
            return String(localized: "Heating", table: "LinkTypes")
        case .other:
            return String(localized: "Other", table: "LinkTypes")
        case .wallBorder:
            return String(localized: "WallBorder", table: "LinkTypes")
        }
    }
}


// MARK: LinkType Identifiable extension
extension LinkType: CaseIterable, Identifiable {
    
    var id: Self { self }
}


// MARK: Link rendering properties.
extension LinkType {
    
    func getMaterial() -> Material {
        switch self {
        case .wire:
            var material = SimpleMaterial()
            material.color = .init(tint: .white.withAlphaComponent(1.0), texture: .init(try! .load(named: "wireTexture")))
            material.metallic = 1.0
            material.roughness = 0.0
            return material
            
        case .pipe:
            return SimpleMaterial(color: UIColor(red: 0, green: 0.54, blue: 1, alpha: 1), isMetallic: false)
        case .gas:
            return SimpleMaterial(color: .yellow, isMetallic: false)
        case .interet:
            return SimpleMaterial(color: .blue, isMetallic: false)
        case .other:
            return SimpleMaterial(color: .white, isMetallic: false)
        default:
            return SimpleMaterial(color: .gray, isMetallic: false)
        }
    }
    
    func getWidth() -> Float {
        switch self {
        case .pipe:
            return 0.008
        default:
            return 0.004
        }
    }
    
    func getRealityModelName() -> String {
        /**
         Returns .reality file name which represents the link type.
         */
        
        switch self {
        case .wire:
            return "Bolt"
        case .pipe:
            return "WaterDrop"
        case .gas:
            return "Flame"
        case .interet:
            return "Internet"
        case .heating:
            return "Heating"
        case .wallBorder:
            return "WallBorder"
        case .other:
            return "Questionmark"
        }
    }
    
    func getSymbol() -> String {
        /**
         Returns SF Symbol name which represents the link type.
         */
        
        switch self {
        case .wire:
            return "bolt.fill"
        case .pipe:
            return "drop.fill"
        case .gas:
            return "flame.fill"
        case .interet:
            return "network"
        case .heating:
            return "pipe.and.drop.fill"
        case .wallBorder:
            return "rectangle"
        default:
            return "questionmark"
        }
    }
}
