//
//  Room.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//

import Foundation

class Room : ObservableObject, Identifiable {
    
    @Published var title: String = ""
    @Published private(set) var walls: [Wall] = []
    
    let created: Date
    var roomDataModel: RoomDataModel? // Related model in the database.
    
    init(
            title: String,
            created: Date = .now,
            roomDataModel: RoomDataModel? = nil,
            save: Bool = false
    ) {
        self.title = title
        self.roomDataModel = roomDataModel
        self.created = created
        if save {
            self.save()
        }
    }
}


// MARK: walls methods
extension Room {
    
    func addWall(wall: Wall, save: Bool = false) {
        walls.append(wall)
        if save {
            self.save()
        }
    }
    
    func setWalls(walls: [Wall], save: Bool = false) {
        self.walls = walls
        if save {
            self.save()
        }
    }
    
    func deleteWalls(offsets: IndexSet) {
        
        // Delete all indexes from the data stack.
        for offset in offsets {
            self.walls[offset].delete()
        }
        
        // Delete view models from the list.
        self.walls.remove(atOffsets: offsets)
    }
}


// MARK: Equatable implementation
extension Room: Equatable{
    
    static func == (lhs: Room, rhs: Room) -> Bool {
        return lhs.id == rhs.id
    }
}


// MARK: Core data inteface
extension Room {
    
    func loadWalls() {
        /**
         When this method is called, walls of this room will be loaded from persistent memory.
         */
        
        // To prevent unwanted reloads, it is checked if the data has been already loaded.
        if self.walls == [] {
            self.walls = PersistenceController.shared.loadWalls(room: self)
        }
    }
    
    func save() {
        
        // Create core data entity if not exists.
        if self.roomDataModel == nil {
            self.roomDataModel = RoomDataModel(context: PersistenceController.shared.container.viewContext)
        }
        
        self.roomDataModel!.save(room: self)
    }
    
    func delete() {
        
        self.roomDataModel?.delete()
    }
}


// MARK: Preview dummy fixtures.
extension Room {
    
    static var empty: Room {
        Room(title: "empty")
    }
    
    static var dummy: Room {
        let room = Room(title: "Dummy room")

        return room
    }
}
