//
//  Wall.swift
//  WallInfo
//
//  Created by Dan Ridzon on 23.03.2023.
//
import Foundation
import RealityKit
import UIKit

class Wall: ObservableObject, Identifiable {
    
    @Published var title: String = ""
    @Published var note: String = ""
    @Published private(set) var images: [ImageModel] = []
    
    let created: Date
    let room: Room
    var wallDataModel: WallDataModel?
    
    private(set) var points: [Point] = []
    
    init(
            title: String,
            room: Room,
            note: String? = nil,
            created: Date = .now,
            wallDataModel: WallDataModel? = nil,
            save: Bool = false
    ) {
        
        self.title = title
        self.note = note ?? ""
        self.created = created
        
        self.room = room
        self.room.addWall(wall: self)

        self.wallDataModel = wallDataModel
        
        if save {
            self.save()
        }
    }
}


// MARK: - images methods
extension Wall {
    
    func saveUIImage(image: UIImage) -> ImageModel {
        /**
         Creates new ImageModel and saves it to the database.
         */
        
        let imageModel = ImageModel(image: image, wall: self, note: "", save: true)
        self.addImage(image: imageModel)
        
        return imageModel
    }
    
    func addImage(image: ImageModel, save: Bool = false) {
        self.images.append(image)
        if save {
            self.save()
        }
    }
    
    func setImages(images: [ImageModel], save: Bool = false) {
        self.images = images
        if save {
            self.save()
        }
    }
    
    func deleteImage(image: ImageModel, save: Bool = false) {
        if let imageIndex = self.images.firstIndex(of: image) {
            self.images.remove(at: imageIndex)
        }
        if save {
            self.save()
        }
    }
    
    func deleteImages(offsets: IndexSet) {
        
        // Delete all indexes from the data stack.
        for offset in offsets {
            self.images[offset].delete()
        }
        
        // Delete view models from the list.
        self.images.remove(atOffsets: offsets)
    }
}


// MARK: - points methods
extension Wall {
    func addPoint(point: Point, save: Bool = false) {
        self.points.append(point)
        if save {
            self.save()
        }
    }
    
    func setPoints(points: [Point], save: Bool = false){
        self.points = points
        if save {
            self.save()
        }
    }
    
    func deletePointReference(point: Point){
        guard let index = self.points.firstIndex(of: point) else {return}
        self.points.remove(at: index)
    }
}


// MARK: Equatable implementation
extension Wall: Equatable{
    
    static func == (lhs: Wall, rhs: Wall) -> Bool {
        return lhs.id == rhs.id
    }
}


// MARK: - Core data inteface
extension Wall {
    
    func save() {
        
        // Create core data entity if not exists.
        if self.wallDataModel == nil {
            self.wallDataModel = WallDataModel(context: PersistenceController.shared.container.viewContext)
        }
        
        self.wallDataModel!.save(wall: self)
    }
    
    func delete() {
        
        self.wallDataModel?.delete()
    }
    
    func loadImages() {
        /**
         When this method is called, images of this wall will be loaded from persistent memory.
         */
        
        // To prevent unwanted reloads, it is checked if the data has been already loaded.
        if self.images == [] {
            self.images = PersistenceController.shared.loadImages(wall: self)
        }
    }
    
    func loadPoints() {
        /**
         When this method is called, AR points of this wall will be loaded from persistent memory.
         */
        
        // To prevent unwanted reloads, it is checked if the data has been already loaded.
        if self.points == [] {
            self.points = PersistenceController.shared.loadPoints(wall: self)
        }
    }
}


// MARK: - Previews dummy fixtures.
extension Wall {
    
    static var dummy: Wall {
        return Wall(title: "dummy", room: .dummy)
    }
}


// MARK: - RealityKit entities.
extension Wall {
    
    func getPointFromChildEntity(entity: Entity?) -> Point? {
        
        guard let entity = entity else { return nil }
        
        for point in self.points {
            for renderedEntity in point.renderedEntities {
                if renderedEntity == entity {
                    return point
                }
            }
        }
        
        return nil
    }
}
