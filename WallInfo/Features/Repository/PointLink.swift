//
//  PointLink.swift
//  WallInfo
//
//  Created by Dan Ridzon on 29.11.2022.
//

import Foundation
import RealityKit

class PointLink: ObservableObject, Identifiable {
    
    let rightPoint: Point
    let leftPoint: Point
    let id: UUID = UUID()
    let linkType: LinkType
    
    private(set) var renderedEntities: [Entity] = []
    private(set) var linkMiddleEntity: Entity?
    
    var pointLinkDataModel: PointLinkDataModel?
    
    init(
            leftPoint: Point,
            rightPoint: Point,
            linkType: LinkType,
            pointLinkDataModel: PointLinkDataModel? = nil,
            save: Bool = false
    ) {
        self.leftPoint = leftPoint
        self.rightPoint = rightPoint
        self.linkType = linkType
        
        self.pointLinkDataModel = pointLinkDataModel
        
        if save {
            self.save()
        }
    }
    
    func destruct(save: Bool = false){
        /**
         This method removes point references to this link and also removes all linked objects on the screen.
         */
        
        // Delete own entities from the screen.
        self.deleteFromScene()
        
        // Remove point references to this object.
        self.rightPoint.deleteLeftLink(link: self)
        self.leftPoint.deleteRightLink(link: self)
        
        if save {
            self.delete()
        }
    }
    
    deinit {
        self.destruct()
    }
    
    func getOtherPoint(point: Point) -> Point {
        /**
         Returns the other point which is also connected by the link.
         */
        
        if point == self.rightPoint {
            return self.leftPoint
        }
        else {
            return self.rightPoint
        }
    }
}


// MARK: RealityKit methods
extension PointLink {
    
    func getLinkMiddle() -> SIMD3<Float> {
        /**
         Returns position at the middle of the link.
         */
        
        let start = self.leftPoint
        let end = self.rightPoint
        
        let middle = SIMD3<Float>((start.position.x + end.position.x)/2, (start.position.y + end.position.y)/2, (start.position.z + end.position.z)/2)
        
        return middle
    }
    
    func deleteFromScene() {
        /**
         Removes all link entities which are rendered on the screen.
         */
        
        self.deleteRenderedEntities()
        self.deleteLinkMiddleEntity()
    }
    
    func deleteRenderedEntities() {
        /**
         Removes link entities which are stored in renderedEntities list. (currently it is just rect of the link itself)
         */
        
        for entity in self.renderedEntities {
            entity.removeFromParent()
        }
        self.renderedEntities = []
    }
    
    private func deleteLinkMiddleEntity() {
        /**
         Removes linkMiddleEntity which is parent of all entities placed at the center of the link.
         */
        
        guard let middleEntity = self.linkMiddleEntity else { return }

        middleEntity.removeFromParent()
        self.linkMiddleEntity = nil
    }
    
    func addRenderedEntity(entity: Entity) {
        guard !self.renderedEntities.contains(entity) else {return}
        self.renderedEntities.append(entity)
    }
    
    func setMiddleEntity(entity: Entity) {
        self.deleteLinkMiddleEntity()
        self.linkMiddleEntity = entity
    }
    
    func getLinkSymbolEntity() -> Entity? {
        /**
         Method tries to load the model entity of the link symbol.
         If the load throws an error, nil is returned.
         */
        
        return try? ModelEntity.load(named: self.linkType.getRealityModelName())
    }
}


// MARK: Equatable implementation
extension PointLink: Equatable{
    
    static func == (lhs: PointLink, rhs: PointLink) -> Bool {
        return lhs.id == rhs.id
    }
}


// MARK: Core data inteface
extension PointLink {
    
    func save() {
        
        // Create core data entity if not exists.
        if self.pointLinkDataModel == nil {
            self.pointLinkDataModel = PointLinkDataModel(context: PersistenceController.shared.container.viewContext)
        }
        
        self.pointLinkDataModel!.save(pointLink: self)
    }
    
    func delete() {
        
        self.pointLinkDataModel?.delete()
    }
}
